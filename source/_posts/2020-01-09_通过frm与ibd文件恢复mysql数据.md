---
title: 通过frm与ibd文件恢复mysql数据
date: 2020-01-09 00:00:00
tags:
- mysql
categories:
- 数据库
---

# frm 文件转化 create table 语句

## 下载并安装 mysql-utilities

### Linux 

```bash
curl http://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-utilities-1.5.3.zip
unzip mysql-utilities-1.5.3.zip
cd mysql-utilities-1.5.3.zip
py ./setup.py build
py ./setup.py install
```

### win 

下载地址：
* x86 32位：http://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-utilities-1.5.3-win32.msi
* x86 64位：http://dev.mysql.com/get/Downloads/MySQLGUITools/mysql-utilities-1.5.3-winx64.msi


## 使用 mysql-utilities

### Linux

```bash
#默认模式
mysqlfrm --server=root:root@localhost --user=root --port=3307  /data/mysql/ht/tb.frm
mysqlfrm --basedir=/usr  --port=3307--user=mysql /data/mysql/ht/tb.frm --show-stats

#诊断模式
mysqlfrm --diagnostic /data/mysql/ht/tb.frm --show-stats
```

### win

```bash
mysqlfrm --basedir=D:/lcgx/data/data/cleared_sino --port=3333 --user=root  D:/lcgx/data/data/cleared_sino/frm/jcgl_cfgl_jcx.frm --show-stats

# win下直接运行py
py mysqlfrm.py --basedir=D:/lcgx/data/data/cleared_sino --port=3333 --user=root  D:/lcgx/data/data/cleared_sino/frm/jcgl_cfgl_jcx.frm --show-stats
py mysqlfrm.py --server=root:@localhost --user=root --port=3307  D:/lcgx/data/data/cleared_sino/frm/jcgl_cfgl_jcx.frm --password
py mysqlfrm.py --diagnostic D:/lcgx/data/data/cleared_sino/frm --show-stats
```

### 更多

直接分析：这种模式比较直接，逐个字节分析 frm 文件，尽可能的提取信息。这种模式下，需要使用 --diagnostic 参数：

```bash
mysqlfrm --diagnostic H:\lcgx\data\data\test\jcgl_jcxx.frm
```

借助 mysql 实例分析：这种模式，借助新的 mysql 实例，从中完成 frm 的分析工作。可以用两种方式来指定，如何开启新的 mysql 实例。

一，从当前的 mysql 服务中 spawn，使用 --server 指定 mysql 服务

```bash
mysqlfrm --server=root:pass@localhost:3306 --port=3310 /data/sakila/actor.frm
```

二，启动新的 mysql 实例，使用 --basedir 指定 mysql 程序路径

```bash
mysqlfrm --basedir=/usr/local/bin/mysql --port=3310 /data/sakila/actor.frm
```

`--port` 给新的实例指定端口，是为了避免与当前的 3306 端口出现冲突。

# 通过 ibd 文件恢复数据

win 下查看data文件夹：

```bash
C:\ProgramData\MySQL\MySQL Server 8.0\Data\
```

frm/ibd 文件操作：

```sql
-- 将原先的.ibd文件与原先的.frm文件解除绑定
use sino_zsk;
alter table jcgl_cfhwk discard tablespace;

-- 新的.ibd文件复制旧的文件，如果提示旧的ibd文件被占用则先关闭服务，删除后再开启服务

-- 将新的.ibd文件与.frm文件发生联系
alter table jcgl_cfhwk import tablespace;
```

强杀线程：

```bash
taskkill /f /im  mysqld.exe
```

注：如果 ibd 文件损坏导致 mysql 服务无法启动，可以在配置文件 mysql.ini 中的 [mysqld] 下添加参数：

```bash
innodb_force_recovery=6
```

innodb_force_recovery参数解释：

- innodb_force_recovery影响整个InnoDB存储引擎的恢复状况，默认值为0，表示当需要恢复时执行所有的恢复操作。当不能进行有效的恢复操作时，mysql有可能无法启动，并记录下错误日志。
- innodb_force_recovery可以设置为1-6,大的数字包含前面所有数字的影响。
当设置参数值大于0后，可以对表进行select,create,drop操作,但insert,update或者delete这类操作是不允许的。
- 1(SRV_FORCE_IGNORE_CORRUPT):忽略检查到的corrupt页
- 2(SRV_FORCE_NO_BACKGROUND):阻止主线程的运行，如主线程需要执行full purge操作，会导致crash
- 3(SRV_FORCE_NO_TRX_UNDO):不执行事务回滚操作。
- 4(SRV_FORCE_NO_IBUF_MERGE):不执行插入缓冲的合并操作。
- 5(SRV_FORCE_NO_UNDO_LOG_SCAN):不查看重做日志，InnoDB存储引擎会将未提交的事务视为已提交。
- 6(SRV_FORCE_NO_LOG_REDO):不执行前滚的操作。


```sql
-- 备份数据
mysqldump -h 192.168.209.136 -uroot -p test> /home/mysql/test.sql

-- 删除数据库
-- 需要先删除对应文件，否则会提示表不存在
drop database test;

-- 去掉参数innodb_force_recovery，重新启动数据库
##innodb_force_recovery=6

-- 导入数据
mysql -h 192.168.209.136 -uroot -pmysql test</home/mysql/test.sql
```