---
title: maven 打包遗漏 mapper 的 xml 文件问题：Invalid bound statement(not found)
date: 2020-05-27 14:32:00
tags:
- java
categories:
- 后端
---

## 背景与分析

微服务从使用 Redis 到引入 mysql 进行数据持久化时，编译正常，启动时 mybatis 报错：` Invalid bound statement (not found)`。经检查，mapper 的相关接口与 xml 文件均没有错误，并非配置错误所造成。

于是检查编译生成的 `target` 文件夹，发现是 xml 文件没有打包进去，原来 eclipse 中会自动打包 mapper 的 xml 文件，使用 IDEA 就需要在 pom,xml 文件中进行声明。

## 解决

在 pom.xml 中增加如下片段：

```xml
<build>
    <!--定义jar包的名字 -->
    <finalName>${project.artifactId}</finalName>
    <resources>
        <resource>
            <directory>src/main/java</directory>
            <includes>
                <include>**/*.xml</include>
            </includes>
            <!-- 是否替换资源中的属性 -->
            <filtering>false</filtering>
        </resource>
        <resource>
            <directory>src/main/resources</directory>
            <includes>
                <include>**/*.yml</include>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
        </resource>
    </resources>
</build>
```