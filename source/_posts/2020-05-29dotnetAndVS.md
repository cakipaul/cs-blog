---
title: .net & visual studio 安装问题
date: 2020-05-29 10:32:00
tags:
- ide
categories:
- 技术资料
---

## 下载离线 VS

### C++桌面开发

官网下载下载器：vs_community2019.exe

```bash
vs_community2019.exe --layout d:\vslayout --add Microsoft.VisualStudio.Workload.NativeDesktop --add Microsoft.VisualStudio.Workload.ManagedDesktop --add Microsoft.VisualStudio.Workload.NetWeb --add Component.GitHub.VisualStudio  --includeRecommended --lang   zh-cn
```

其中 `d:\vslayout` 即离线文件目录。下载好后可以使用 ultraISO 将文件夹制作成 iso 文件。

## VS2017无法进入安装界面、闪退问题

参考：https://blog.csdn.net/qq951127336/article/details/71036868

通过 任务管理器的进程中可以看到 bootstrap 进程，因为是临时进程，会很快消失，所以手速要快。看到之后快速右键，打开所在文件夹，一般是tmp文件夹，然后文件周围一定有log文件。

这是我们的突破口，打开log阅读内容，可以找到自己的错误是什么。题主的错误是 .NET 中一个方法错了，所以有exception,所以我猜自己.NET FRAMWORK 有问题，所以把.net 框架 全卸载后重新安装了一下。重启电脑后就可以正常下载了。

NET之前有中途安装强退过，可能与此有关，导致文件受损。于是使用 .NET Framework 修复工具：[NetFxRepairTool.exe](https://support.microsoft.com/zh-cn/help/2698555/microsoft-net-framework-repair-tool-is-available)


## 网络导致下载问题

第一步：检查BITS服务及相关依赖服务是否正常开启，如果没开启则全部设为自动并启用（这里我确实有一些服务没有开启）

Remote Procedure Call (RPC)
COM+ Event System
System Event Notification Services
Background Intelligent Transfer Service-----》这个就是BITS服务
Network Connections

第二步：如果开启了上述所有的服务仍然不能解决问题，则需要检查是否存在组件缺失，若存在则需要修复，如下：（我的组件没有缺失）
Windows+x，打开“命令提示符（管理员），执行以下命令：

```bash
DISM/Online /Cleanup-image /Scanhealth
DISM/Online /Cleanup-image /Restorehealth
Sfc /scannow
```

第三步：如果执行了第二步检查并修复了相关组件后还有问题，则需要检查Socket组件，必要时执行如下命令进行重置（我的就需要重置）

```bash
netsh winsock reset
netsh int ip reset
ipconfig /release
ipconfig /renew
ipconfig /flushdns
```

执行完上述三步后，重启电脑。

## 卸载并重新安装 .NET 3

在 Windows 8 和更高版本的操作系统上，.NET Framework 是一个操作系统组件，不能单独卸载。如果要强行卸载并重新安装 .net framework，需要用 cleanup_tool 与 win10 镜像。

文档：https://docs.microsoft.com/zh-cn/archive/blogs/astebner/net-framework-cleanup-tool-users-guide

安装 .NET3 ：用管理员运行cmd 命令 

```bash
dism.exe /online /enable-feature /featurename:NetFX3 /Source:H:\sources\sxs /LimitAccess
```

其中 `H:` 是打开 iso 后生成的虚拟光驱盘符。启动成功后重启电脑。

更多离线安装 .net 的方法：[https://zhuanlan.zhihu.com/p/33467631](https://zhuanlan.zhihu.com/p/33467631)