---
title: 使用 Scriptable 实现 IOS 自定义小组件
date: 2020-10-10 16:18:00
tags:
- ios
categories:
- 前端
---

## Scriptable 简介

- Plain JavaScript: Supports JavaScript ES6. Scripts are stored as plain JS files on disk.
- Native APIs: Integrate with the native APIs of iOS directly from JavaScript.
- Siri Shortcuts: Run scripts from Siri Shortcuts. Present tables, websites, HTML and more in Siri.
- Documentation: All native APIs that are bridged to JavaScript have documentation which is available offline.
- Share Sheet Extension: Run a script from a share sheet and process the inputs.
- Files Integration: Integrated with the file system and Files.app enabling you to perform operations on files.
- Customizable: The editor can be customized to match your preferences.
- Example scripts: Comes with several example scripts to get you started.
- x-callback-url: Communicate with other apps using x-callback-url.
- Made by Simon Støvring in Copenhagen； Scriptable uses Apples JavaScriptCore

- [官方文档](https://docs.scriptable.app/)
- [ios JavaScript Core](https://developer.apple.com/documentation/javascriptcore)
- [一篇简介文章](https://redirect.ruguoapp.com/?redirect=https%3A%2F%2Fwww.wolai.com%2Finfinity%2Fnx9JRmXSXiG9WbrtPGJ8Qz)

## 使用天气小组件

- [天气小组件使用说明与代码](https://github.com/xkerwin/Scriptbale/tree/main/Weather)
- [js 代码](https://raw.githubusercontent.com/xkerwin/Scriptbale/main/Weather/Hello-Weather.js)

效果图：

![scriptable-weather](/cs-blog/img/scriptable-weather.png)

## 附：open weather 摘要说明

调用 Api:

`let wetherurl = "http://api.openweathermap.org/data/2.5/weather?id=" + CITY_WEATHER + "&APPID=" + APP_ID + "&units=metric"`

华氏度设置为英制 imperial，摄氏度设置为公制 metric。其中部分 `CITY_WEATHER` 代码如下：

```js
// 北京 1816670
// 上海 1796236
// 广州 1809858
// 深圳 1795565
// 南京 1799962
// 苏州 1886760
// 厦门 1790645
// 济南 1805753
let CITY_WEATHER = "1805753";
```

在此处获取免费的API密钥：[https://openweathermap.org/appid](https://openweathermap.org/appid)

示例：

`http://api.openweathermap.org/data/2.5/weather?id=1805753&APPID=********&units=metric`

返回值：

```json
{
    "coord": {
        "lon": 117,
        "lat": 36.67
    },
    "weather": [
        {
            "id": 500,
            "main": "Rain",
            "description": "light rain",
            "icon": "10d"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 21.67,
        "feels_like": 19.85,
        "temp_min": 21.67,
        "temp_max": 21.67,
        "pressure": 1013,
        "humidity": 50
    },
    "visibility": 10000,
    "wind": {
        "speed": 2.97,
        "deg": 230
    },
    "rain": {
        "1h": 0.25
    },
    "clouds": {
        "all": 76
    },
    "dt": 1602314886,
    "sys": {
        "type": 3,
        "id": 2020829,
        "country": "CN",
        "sunrise": 1602281682,
        "sunset": 1602322978
    },
    "timezone": 28800,
    "id": 1805753,
    "name": "Jinan",
    "cod": 200
}
```