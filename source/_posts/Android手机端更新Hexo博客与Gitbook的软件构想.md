---
title: 手机端更新Hexo博客与Gitbook的软件构想
date: 2019-01-09 11:30:32
tags:
- gitbook
- hexo
- design
categories:
- 前端
---
# 基础：
- 需要云主机
- 安装Node.js,Git环境
- 安装并配置了Hexo,Gitbook

# 基本思路
- Github作为编辑与备份的库
- Hexo/GitBook Repo
- Hexo发布到userName.github.io
- GitBook发布到个人网站

# Hexo
- 读取Hexo repo里的*.md文件
- Markdown编辑/快捷组件/预览界面
	- description中title/tags/categories等管理
	- 所见即所得或标准markdown编辑器
	- 配置管理？_config.yml
- 编辑后同步到github
- 发布功能
	- ssh连接云主机
	- 同步github repo
	- 运行hexo clean&generate&deploy命令

# GitBook
- 读取GitBook repo里的文件
- 菜单式的文件组织界面
- 插件管理？book.json
- Markdown编辑/快捷组件/预览界面
	- description管理
	- 所见即所得或标准markdown编辑器
- 编辑后同步到github
- 发布功能
	- ssh连接云主机
	- 同步github repo
	- 运行gitbook build命令

Ps.cache time settings
