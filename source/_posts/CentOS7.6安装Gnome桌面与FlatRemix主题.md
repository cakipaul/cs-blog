---
title: CentOS7.6安装Gnome桌面与FlatRemix主题
date: 2019-02-01 12:57:00
tags:
- linux
- gnome
categories:
- 操作系统
---
# 1 Gnome
## 1.1 Gnome 简介
GNOME（读音是 gah-nohm 或 nohm）是一个简单易用的桌面环境。它是由 GNOME 项目设计并完全由自由和开源的软件组成。GNOME 是 GNU 项目的一部分。默认使用 Wayland 而不是 Xorg 进行显示。

详情查看： [Gnome中文手册](https://wiki.archlinux.org/index.php/GNOME_%28%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87%29#.E9.87.8D.E5.90.AF_GNOME_shell)

## 1.2 安装 Gnome
安装Gnome需要使用root账户进行操作
```bash
# 1 安装X Windows System
yum groupinstall "X Window System" 

# 2 查看已安装及可安装的grouplist,并安装其中的"GNOME Desktop"与"Graphical Administration Tools"
yum grouplist
yum groupinstall "GNOME Desktop" "Graphical Administration Tools"

# 3 进入gnome
startx
```
*注意：若是使用虚拟机，要注意虚拟机内存至少分配1024M。*

## 1.3 使用 Shell
1. 若想从桌面退回到命令行，快捷键为：
    ```bash
    Ctrl + Alt + F1 
    # 若按F1没反应，换为F2~F6中的任意一个都行
    ```
2. 进入Gnome下的终端：

    桌面左上角处 Applications - System Tools - Terminal，或者在桌面点击右键，选择 Open Terminal。
3. 设置快捷键打开终端：

    Applications - System Tools - Settings - Devices - Keyboard

    在 Keyboard Shortcuts 中，点击下方加号添加新的快捷键：
    ```bash
    name: Run Terminal
    command: /usr/bin/gnome-terminal
    shortcut: Ctrl + T
    ```
    点击 Add 添加该快捷键，这样，使用 Ctrl + T 就可以打开终端了。

## 1.4 开启/关闭/启动项
1. 设置系统默认启动选项为图形界面:
    ```bash
    ln -sf /lib/systemd/system/runlevel5.target /etc/systemd/system/default.target
    ```
2. CentOS 6.x

    ```bash
    # 1 关闭图形界面
    init 3   # 关闭图形界面（XServer服务也会关闭）

    # 2 开启图形界面
    init 5
    # 或者
    startx  # 进入第一个图形界面（如果有多个的话）

    # 3 开机时，默认不进入 X Window
    vim /etc/inittab
    #找到 id:5:initdefault:
    #改成 id:3:initdefault:
    ```

3. CentOS 7.x：

    CentOS 7 中不再使用 inittab ，而是使用 systemd ：

    ```bash
    # 查询当前所设定的状态
    systemctl get-default
    # multi-user.target 相当于 level 3，也就是命令行终端；而 graphical.target 相当于 level 5，也就是图形界面。

    # 设置默认启动到图形界面
    systemctl set-default graphical.target

    # 设置默认启动到命令行
    systemctl set-default multi-user.target
    ```

# 2 Flat Remix
## 2.1 Gnome 主题简介
gnome 通过 gnome-tweak-tool(优化工具) 来管理主题。可以在 Applications - Accessories - Tweaks 中打开，也可以直接在终端输入 gnome-tweak-tool 来启动它。

1. 存放主题的目录：

    /usr/share/themes (公共)

    ~/.themes(用户)  

2. 存放图标、光标样式的目录：

    /usr/share/icons   

3.  存放扩展的目录：

    /usr/share/gnome-shell/extentions（公共） 
    
    ~/.local/share/gnome-shell/extentions(用户)

Flat Remix GNOME 主题是一款基于 Material Design 扁平化主题，整体布局遵循现代化“扁平设计”理念，有着精致的图标、优雅的动态效果、较高的对比度与清晰的边界，是一款值得尝试的Gnome主题。

![flat-remix-gnome](/cs-blog/img/flat-remix-gnome.jpg)

更多展示截图参考：[flat-remix-gnome:Github](https://github.com/daniruiz/flat-remix-gnome/blob/master/README.md)

## 2.2 终端安装 Flat Remix
```bash
cd /tmp

# 若曾经安装过，则先清除历史文件
rm -rf flat-remix-gnome

# 通过git进行安装，若无git，则先安装git
git clone https://github.com/daniruiz/flat-remix-gnome
cd flat-remix-gnome
sudo make install
sudo mv /usr/share/gnome-shell/gnome-shell-theme.gresource /usr/share/gnome-shell/gnome-shell-theme.gresource.old
sudo ln /usr/share/themes/Flat-Remix/gnome-shell-theme.gresource /usr/share/gnome-shell/gnome-shell-theme.gresource
```
运行完上述操作后，在Gnome终端种运行以下命令即可切换主题：
```bash
gsettings set org.gnome.shell.extensions.user-theme name "Flat-Remix"
```
注意需要先在 Tweaks 中设置允许 Shell 主题：Applications - Accessories - Tweaks 打开 Tweaks 管理工具，选择 Extensions-User themes，将默认的 OFF 打开为 ON 即可。

设置好后重启桌面：`Alt + F2` ，然后在弹出框中输入 `r` 即可。

Flat Remix 主题中有多款子主题，可以在 Tweak 管理器的 Appearance - Themes - Shell 中设置。

# 3 通过VNC连接云服务器
## 3.1 VNC简介
VNC (Virtual Network Console)中文译名为虚拟网络控制台。它是由著名的 AT&T 的欧洲研究实验室开发的一款优秀的远程控制工具软件，可以提供图像化的远程桌面连接。

VNC系统由客户端，服务端和一个协议组成。VNC的服务端目的是分享其所运行机器的屏幕， 服务端被动的允许客户端控制它。 VNC客户端（或Viewer） 观察控制服务端，与服务端交互。 VNC 协议 Protocol (RFB)是一个简单的协议，传送服务端的原始图像到客户端（一个X,Y 位置上的正方形的点阵数据）， 客户端传送事件消息到服务端。

## 3.2 云服务器安装VNC Server

```bash
# CentOS 6.x 下安装 vnc-server
# CentOS 7.x 下安装 tigervnc-server
yum install tigervnc-server

# 设置密码
vncserver

# 设置防火墙
iptables -I INPUT -p tcp --dport 5901:5902 -j ACCEPT
iptables -I INPUT -p udp --dport 5901:5902 -j ACCEPT

# 启动 VNC Server
vncserver :1

# 关闭 VNC Server
vncserver -kill :1
```

## 3.3 本地安装VNC Viewer
首先下载并安装 NVC Viewer 客户端：[下载 VNC Viewer](https://www.realvnc.com/en/connect/download/viewer/)

安装后通过 File-New Connection 新建连接，输入 `<云服务器ip>:5901` 与自定义名称即可建立连接，连接时需要密码验证，输入 VNC Server 设置的密码即可。

# 4 资源参考
- [Gnome官网](https://www.gnome.org)
- [Gnome中文手册](https://wiki.archlinux.org/index.php/GNOME_%28%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87%29#.E9.87.8D.E5.90.AF_GNOME_shell)
- [Flat Remix 官网](https://drasite.com/flat-remix-gnome)
- [flat-remix-gnome GitHub](https://github.com/daniruiz/flat-remix-gnome)
- [VNC 官网](https://www.realvnc.com/)
