---
title: Git-Node.js-Hexo-Gitbook环境搭建与使用指南
date: 2019-01-07 13:03:55
tags:
- node.js
- git
- hexo
- gitbook
categories:
- 前端
---
# 1. 安装Git
## 1.1 什么是Git
[Git官方文档](https://git-scm.com/doc)

## 1.2 安装Git
win：安装GitBash

CentOS：`yum install git`

更多git命令可以查看[runoob-git](http://www.runoob.com/git/)

# 2. 安装NVM
## 2.1 什么是NVM
Node Version Manager
[nvm-github](https://github.com/creationix/nvm#git-install)

## 2.2 设置国内镜像
```bash
#设置nvm使用淘宝镜像下载node
export NVM_NODEJS_ORG_MIRROR=https://npm.taobao.org/mirrors/node

#安装好npm后使用cnpm代替npm
npm install cnpm -g --registry=https://registry.npm.taobao.org
#安装好后就可以使用cnpm代替npm
```

## 2.3 安装/管理node|npm
[nvm、node、npm的安装与使用](https://www.kancloud.cn/summer/nodejs-install/71975)
[使用NVM管理不同版本node与npm](http://bubkoo.com/2017/01/08/quick-tip-multiple-versions-node-nvm/)

# 3. 安装Hexo
## 3.1 相关链接
[Hexo官网](https://hexo.io/zh-cn)

[Hexo-github](https://github.com/hexojs/hexo)

## 3.2 搭建过程
[Hexo搭建步骤官方文档](https://hexo.io/zh-cn/docs/)

[腾讯云搭建Hexo](https://zhuanlan.zhihu.com/p/34400760)

[Hexo常见问题解决方案](https://xuanwo.io/2014/08/14/hexo-usual-problem/)


## 3.3 主题
[next-github](https://github.com/iissnan/hexo-theme-next)
*注：主题的安装与配置说明在README文件中*

## 3.4 插件
[插件官网](https://hexo.io/plugins/)


1. 部署到git的插件：[hexo-deployer-git](https://github.com/hexojs/hexo-deployer-git)
2. mermaid：[hexo-filter-mermaid-diagrams](https://github.com/webappdevelp/hexo-filter-mermaid-diagrams)
3. 本地搜索插件：`npm install hexo-generator-searchdb --save`

```

# 4. 安装GitBook
## 4.1 GitBook使用说明

[GitBook简明教程](https://colobu.com/2014/10/09/gitbook-quickstart/)

[GitBook中文手册](https://gitbook.zhangjikai.com/)

[GitBook插件介绍](https://yangjh.oschina.io/gitbook/faq/Plugins.html)
## 4.2 GitBook相关链接

[GitBook官网](https://www.gitbook.com)

[GitBook-github](https://github.com/GitbookIO/gitbook)

[下载GitBook Editor](https://legacy.gitbook.com/editor)

[ToolChain文档](https://toolchain.gitbook.com)

# 5. 使用Git更新文章
## 5.1 使用Git更新Hexo
本站将Hexo博客更新到了cakipaul.github.io上。在hexo的source文件夹中添加CNAME文件（无后缀），使用记事本打开并添加`blog.cakipaul.com`，然后编辑hexo的_congig.yml文件，将url修改为blog.cakipaul.com，然后运行：
```bash
hexo clean
hexo g -d
```
就可以将静态网页发布到cakipaul.github.io上了，注意还要在cakipaul.com的域名解析中添加CNAME记录，主机名即blog，这样使用blog.cakipaul.com域名就也也可以访问cakipaul.github.io了。

另外我在github仓库中新建了hexoblog库，在云主机的hexo根目录中输入命令：
```bash
#初始化git
git init

#添加源文件与配置文件
git add source/
git add _config.yml

#添加远程主机,命名为github
git remote add github git@github.com:cakipaul/hexoblog.git

#添加第一次commit
git commit -m "first commit"

#push到github
git push -u github master
#注意这一步需要将云主机的ssl公钥添加到github中，就可以免密ssh连接github了。详情搜索“github添加ssh”
```
在windows系统中安装Github客户端来进行版本控制，并安装VS CODE，就可以用精美的界面编辑markdown文件了~
每次文件更新后运行push命令，并在云主机上pull一下，然后再运行`hexo g -d`，就可以使用云主机向github部署博客了。

如果是使用android手机，还可以在谷歌市场下载Termux，还有TermuxTutorial手册，就可以在安卓手机上尽情享受终端~~的快感~~了。另外还需要有一个带esc,ctrl等功能的键盘，推荐使用Hacker's KeyBoard，当然最好还是使用OTG接口外插一个键盘~(或者直接使用蓝牙键盘~)

Termux使用`pkg`命令对包进行管理（或者apt命令），可以使用`pkg openssh`安装ssh链接模块，就可以ssh连接云主机了。之后的操作就是一番git commit|push|pull、hexo clean|generate|deploy命令，就可以在github部署自己的网页了。当然使用手机来敲命令还是蛮反人类的，~~可能未来会考虑做一个Hexo博客同步、编辑与发布的手机客户端~~！

## 5.2 使用Git更新GitBook
gitbook的运行与hexo一般，是基于node.js与git环境。使用npm命令（或cnmp命令）安装gitbook-cli后，可以用`gitbook fetch version`来安装以前的版本。更多命令还是参考官网手册：[ToolChain文档](https://toolchain.gitbook.com)。

另外有一位极其认真的兄台针对gitbook从2.x.x到3.x.x的各种兼容问题制作了非常详细的中文说明手册，从环境部署到插件安装一应俱全：[GitBook中文手册](https://gitbook.zhangjikai.com/)，在此就不再赘述了。