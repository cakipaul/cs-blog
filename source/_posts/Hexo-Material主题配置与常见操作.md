---
title: Hexo-Material主题配置与常见操作
date: 2019-01-16 14:57:00
tags:
- hexo
- hexo-theme-material
categories:
- 前端
---
# 1 Material主题简介
## 1.1 安装与基本配置

- 源代码：[material-github](https://github.com/viosey/hexo-theme-material)
- 文档：[material-docs](https://github.com/neko-dev/material-theme-docs)

>注意：该主题目录下的`_config.yml`文件被命名为`_config.template.yml`，需要自行复制并重命名为`_config.yml`，然后再进行配置。这是为了避免版本更新带来配置文件损坏。

配置文件的设置方法参考：[docs](https://github.com/neko-dev/material-theme-docs/tree/master/docs) 中的 `_config-*.md` 文件。

# 2 建立多相册
## 2.1 相册系统目录分析
material主题自己提供了gallery功能，但是只能建立一个gallery页面。默认图库建立的配置方法可以参考[Doc](https://github.com/neko-dev/material-theme-docs/blob/master/docs/pages.md)文档中“创建「图库」页面”。



## 2.2 修改layout.ejs
打开`./themes/material/layout/`文件夹，编辑`layout.ejs`文件，发现在文件开始部分有这样的代码：

```js
<% if(page.layout === 'gallery') { %>
    <!-- Single Gallery Page -->
    <%- partial('_widget/page-gallery') %>
<% } else { %>

...

<% } %>
```

我们可以新建一个名称为sadfrog的layout，嵌套在判断语句之中：

```js
<% if(page.layout === 'gallery') { %>
    <!-- Single Gallery Page -->
    <%- partial('_widget/page-gallery') %>
<% } else { %>
    <% if(page.layout === 'sadfrog') { %>
        <!-- Single Gallery Page -->
        <%- partial('_widget/page-frog') %>
    <% } else { %>

    ...

    <% } %>
<% } %>
```
之后需要在主题目录下的`./layout/_widget/`文件夹下新建page-sadfrog.ejs文件。

## 2.3 新建page-sadfrog.ejs文件
将`./layout/_widget`目录下的`page-gallery.ejs`文件复制一份，命名为`page-sadfrog.ejs`。打开该文件进行编辑，发现以下读取配置文件的代码：
```js
<% if (site.data.gallery) { %>
    <% for (var i in site.data.gallery) { %>
        <article class="thumb">
            <a href="<%= site.data.gallery[i].full_link %>" class="image lazy" data-original="<%= site.data.gallery[i].thumb_link %>"><img class="lazy" data-original="<%= site.data.gallery[i].thumb_link %>" alt="<%= i %>" /></a>
            <h2><%= i %></h2>
            <p><%= site.data.gallery[i].descr %></p>
        </article>
    <% } %>
<% } %>
```

将其中的`site.data.gallery`字段替换为`site.data.sadfrog`，这样就会读取hexo`./source/_data`文件夹中的`sadfrog.yml`配置文件了。
另外默认排序为升序排列，在此略微修改js代码实现降序排列效果：

```js
<% if (site.data.sadfrog) { %>
    <% var arr = new Array() %>
    <% var num = 0 %>
    <% for (var i in site.data.sadfrog) { %>
        <% arr[num]=i %>
        <% num=num+1 %>
    <% } %>
    <% for (num=num-1;num>=0;num--) { %>
        <article class="thumb">
            <a href="<%= site.data.sadfrog[(arr[num])].full_link %>" class="image lazy" data-original="<%= site.data.sadfrog[(arr[num])].thumb_link %>"><img class="lazy" data-original="<%= site.data.sadfrog[(arr[num])].thumb_link %>" alt="<%= i %>" /></a>
            <h2><%= arr[num] %></h2>
            <p><%= site.data.sadfrog[(arr[num])].descr %></p>
        </article>
    <% } %>
<% } %> 
```

## 2.4 编辑sadfrog.yml文件
在hexo的`./source/_data`目录中新建`sadfrog.yml`配置文件，编辑器中内容：

```
Name:
  full_link: http://example.com/full-image.png
  thumb_link: http://example.com/thumb-image.png
  descr: "这是一个描述"
```
- Name: 图片名字，例如 Material；
- thum_link：缩略图url，也可以是source中的文件夹，如`/img/sadfrog/thumbnail`；
- descr：图片描述。

添加多张图片，只需要根据上面的格式重复填写即可。

## 2.5 编辑其他配置文件
在hexo博客的source文件夹中新建sadfrog文件夹，并在里面添加index.md文件，编辑内容为：
```md
---
title: Sadfrog
date:
layout: sadfrog
---
```
其中title自行拟定，layout即使用了刚刚创建的`page-sadfrog`布局。

编辑主题目录下的`_config.yml`文件，在`sidebar:`中的`pages:`一栏下添加：
```
SadFrog:
    link: "/sadfrog/"
    icon: format_paint
    divider: false
```
其中icon可以在material的icon网站查看：[icon](https://material.io/tools/icons/?icon=border_color&style=baseline)

这样就完成了自定义相册的配置了。

# 3 配置文章展示图片
## 3.1 使用自定义缩略图
参考文档：[docs-compose](https://github.com/neko-dev/material-theme-docs/blob/master/docs/compose.md)
在 Material 主题中，每个 Scheme 都有缩略图功能。 只需要在 Front-matter 中添加参数 `thumbnail:` ，然后填入缩略图地址即可。


## 3.2 使用随机缩略图
Material主题配置文件中可以设置两种scheme，不同的scheme有不同的缩略图机制：

- Paradox：此 Scheme 如果没有自定义缩略图，则使用默认随机缩略图。编辑主题目录的 `_config.yml` 文件：
```
thumbnail:
    purecolor:
    random_amount: 
```
将 `purecolor` 项置空，在 `random_amount` 项中填写使用的随机图片数目。所使用的图片目录默认位于 `source/img/random` 下，命名格式为 `material-<num>.png` 。也可以通过编辑配置文件来修改thumbnail的文件目录与命名规则：
```
img:
    logo: "/img/logo.jpg"
    avatar: "/img/avatar.jpeg"
    daily_pic: "/img/daily_pic.jpg"
    sidebar_header: "/img/sidebar_header.jpg"
    random_thumbnail: "/img/random/pic-"
```
编辑 `random_thumbnail` 条目， `/img/random/` 为文件目录， `pic-`为命名前缀，也可以将其删除。

- Isolation：此 Scheme 只会显示已自定义缩略图。

## 3.3 修改随机缩略图文件格式
在使用随机缩略图时，默认的文件目录为 `./source/img/random/` ，文件名前缀为 `material-` ，文件格式为 `*.png`。但是 `png` 格式的文件占用较大的流量，期望使用 `jpg` 格式的随机缩略图作文章展示图片，这就需要编辑thumbnail相关的配置文件。

编辑`.\layout\_partial\` 目录下的 `Paradox-post_entry-thumbnail.ejs` 和 
`Paradox-post-thumbnail.ejs` 文件，其中各有一处“ png ”字段，将其替换为“ jpg ”即可。

# 4 修改blog_info模块
## 4.1 blog_info.ejs文件
该模块的 `ejs` 文件为 `./layout/_partial/blog_info.ejs` 。编辑此文件，可以看到该模块共分为三个部分：search、logo与information。

## 4.2 修改logo样式
logo默认样式有一圈留白，通过修改 `blog_info.ejs` 文件的 logo 部分的代码可以修改它的样式：

```js
<!-- LOGO -->
<div class="something-else-logo mdl-color--white mdl-color-text--grey-600" style="background-image:url(<%= url_for(theme.img.logo) %>)" onclick="window.location='<%= theme.url.logo || '#' %>';">
</div>
```

这样通过把logo图片设置为背景图片了而取消了四周留白。另外点击图片导向url的方法通过 `onclick` 实现。

**注：**移动端显示logo时宽高比比较大，logo的下半部分可能显示不全，要注意提前裁剪好logo图片。

# 5 修改文本颜色
## 5.1 post-nav文件页脚导航
由于 [blog.cakipaul.com](blog.cakipaul.com)站点采用了暗色背景图片，而页脚文字的默认颜色为深灰色，通过修改配置文件可以把它改为白色：

修改页脚导航栏的ejs文件：`\themes\material\layout\_partial\post-nav.ejs`
里面有两个 `Nav`(导航) 定义，分别是 `<!-- Prev Nav -->` 与 `<!-- Next Nav -->` 。其中各有一段文本代码，将其分别用 `<span>` 标签包裹，并添加 `class="mdl-color-text--white"` 属性：
```js
<span class="mdl-color-text--white"><%= __('post.newer') %></span>

<span class="mdl-color-text--white"><%= __('post.olderer') %></span>
```
这样即可将“前/后一篇文章”的导航文本置为白色。

## 5.2 search文本
设置过程类似于 [5.1-post-nav文件页脚导航](#5.1-post-nav文件页脚导航) ，但是值得注意的是，目前只有 `Paradox` 主题支持Searcher， `Isolation` 主题没有Searcher。而具体使用的Searcher需要在主题的配置文件 `themes\material\_config.yml` 中配置：
```yml
# Search Systems
# Available value:
#     swiftype | google | local
search:
    use: google
    swiftype_key:
```

ejs文件为 `themes\material\layout\_partial\Paradox-search.ejs` 。以其中的google为例：

```js
<% if( theme.search.use === 'google' ) { %>
    <!-- Google -->
    <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable" method="post" action="">
        <label id="search-label" class="mdl-button mdl-js-ripple-effect mdl-js-button mdl-button--fab mdl-color--accent mdl-shadow--4dp" for="search">
            <i class="material-icons mdl-color-text--white" role="presentation">search</i>
        </label>

        <form autocomplete="off" id="search-form" method="get" action="//google.com/search" accept-charset="UTF-8" class="mdl-textfield__expandable-holder" target="_blank">
            <input class="mdl-textfield__input search-input" type="search" name="q" id="search" placeholder="">
            <label id="search-form-label" class="mdl-textfield__label" for="search"></label>

            <input type="hidden" name="sitesearch" value="<%= config.url %>">
        </form>
    </div>
<% } %>
```

在其中 `input` 部分的 `class` 中添加 `mdl-color-text--white` ：

```js
<input class="mdl-textfield__input search-input mdl-color-text--white" type="search" name="q" id="search" placeholder="">
```

这样在searcher中输入的文字就会是白色了。其他类型的searcher的设置方法与之同理。