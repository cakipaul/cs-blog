---
title: Nginx服务器实现https连接
date: 2019-01-11 13:00:00
tags:
- nginx
- https
categories:
- 网络
---
# 1 申请并安装证书
在云平台注册域名并认证后，可以在云平台申请SSL证书，一般腾讯云和阿里云都可以免费申请一年的证书。申请后下载压缩文件，其中有各种服务器所对应的文件夹，内有\*.crt与\*.key文件。本站服务器使用的nginx，解压两个文件并上传到云服务器的`/usr/local/nginx/cert/`文件夹中。

# 2 配置
## 2.1 配置nginx.conf文件


将http解析到https上：

```bash
server {
    listen       80;
    server_name  cakipaul.com www.cakipaul.com;
    return 301 https://$server_name$request_uri;
}
```

添加https服务：
```bash
server {
    # 服务器端口使用443，开启ssl, 这里ssl就是上面安装的ssl模块
    listen       443 ssl; #http2 default_server;
    listen       [::]:443 ssl; #http2 default_server;
    # 域名，多个以空格分开
    server_name  cakipaul.com www.cakipaul.com;
    ssl on;

    # ssl证书地址
    ssl_certificate     /usr/local/nginx/cert/ssl.crt;  # crt文件的路径
    ssl_certificate_key  /usr/local/nginx/cert/ssl.key; # key文件的路径

    # ssl验证相关配置
    ssl_session_timeout  5m;    #缓存有效期
    ssl_ciphers HIGH:!aNULL:!MD5;    #加密算法
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;    #安全链接可选的加密协议
    ssl_prefer_server_ciphers on;   #使用服务器端的首选算法

    #之后就是root与location配置
}
```

## 2.2 设置防火墙
```bash
#添加端口白名单
firewall-cmd --permanent --zone=public --add-port=443/tcp

#重启防火墙
firewall-cmd --reload
```

# 参考
[网站(Nginx)配置 HTTPS 完整过程](https://www.centos.bz/2018/12/%E7%BD%91%E7%AB%99nginx%E9%85%8D%E7%BD%AE-https-%E5%AE%8C%E6%95%B4%E8%BF%87%E7%A8%8B/)

[Centos7下nginx配置https](https://blog.csdn.net/chunyuan314/article/details/77369110)