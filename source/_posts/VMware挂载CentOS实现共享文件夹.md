---
title: VMware挂载CentOS实现共享文件夹
date: 2019-01-11 13:00:00
tags:
- linux
- vmware
categories:
- 操作系统
---
# 1 安装VMwareTools
[在 Windows 虚拟机中手动安装 VMware Tools](https://docs.vmware.com/cn/VMware-Tools/10.2.0/com.vmware.vsphere.vmwaretools.doc/GUID-391BE4BF-89A9-4DC3-85E7-3D45F5124BC7.html)

[在 Linux 虚拟机中手动安装 VMware Tools
](https://docs.vmware.com/cn/VMware-Tools/10.2.0/com.vmware.vsphere.vmwaretools.doc/GUID-08BB9465-D40A-4E16-9E15-8C016CC8166F.html)

# 2 添加共享文件夹

开始之前：
1. 首先要将所有使用该虚拟磁盘的虚拟机关机。
2. 确认该虚拟磁盘上的虚拟磁盘 (.vmdk) 文件未被压缩，且不具有只读权限。
3. 在 Windows 主机中，确认卷已格式化为 FAT (12/16/32) 或 NTFS 类型。只有 FAT (12/16/32) 和 NTFS 格式的卷受支持。如果虚拟磁盘包含混合分区，例如一个分区使用 Linux 操作系统进行格式化，另一个分区使用 Windows 操作系统进行格式化，则您只能映射 Windows 分区。
4. 确认该虚拟磁盘未加密。您无法映射或装载加密的磁盘。

选择 Player > 管理 > 虚拟机设置。在选项选项卡中，选择共享文件夹,选择一个文件夹共享选项。

# 3 查看共享文件夹
在 Linux 客户机中，共享文件夹位于 /mnt/hgfs 下。在 Solaris 客户机中，共享文件夹位于 /hgfs 下。

# 4 Q&A:共享文件夹目录为空

- 方法一: 卸载并重装VMwareTools

- 方法二：使用脚本挂载
    1. 首先保证你安装了vmware，并且安装了kernel相关:
    ```bash
    yum install kernel kernel-headers kernel-devel -y
    ```
    2. 使用以下脚本挂载共享文件夹：
    ```bash
    #!/bin/bash

    vmware-hgfsclient | while read folder; do
        echo "[i] Mounting ${folder}   (/mnt/hgfs/${folder})"
        mkdir -p "/mnt/hgfs/${folder}"
        umount -f "/mnt/hgfs/${folder}" 2>/dev/null
        vmhgfs-fuse -o allow_other -o auto_unmount ".host:/${folder}" "/mnt/hgfs/${folder}"
    
    done
    
    sleep 2s
    ```

- 方法三: 运行指令关联文件夹：
    ```bash
    #将共享文件夹显示在/share/目录中
    sudo vmhgfs-fuse .host:/ /share

    #完整命令：
    sudo vmhgfs-fuse -o allow_other -o auto_unmount -o uid=1000 -o gid=1000 .host:/shared_folder /mnt/shared_folder
    #其中shared_folder为你添加的共享文件夹名字，/mnt/shared_folder为虚拟机中的共享文件夹挂载路径，需要手动创建。
    ```

- 方法四：使用脚本实现开机自动挂载：

    在/etc/fstab文件的最后添加如下内容实现将所有共享文件夹自动挂载到目录/share：
    ```bash
    .host:/  /share  fuse.vmhgfs-fuse allow_other,uid=1000,gid=1000,auto_unmount,defaults 0 0
    ```
    这样开机之后所有你添加的共享文件夹，都会以子文件夹的形式挂载在目录/share中。
    
    *注意:/share/文件夹需存在*

# 5 参考
[VMware官方手册](https://docs.vmware.com)

[为虚拟机启用共享文件夹](https://docs.vmware.com/cn/VMware-Workstation-Player/12.0/com.vmware.player.win.using.doc/GUID-D6D9A5FD-7F5F-4C95-AFAB-EDE9335F5562.html)

[vmware 共享文件夹hgfs下为空的解决办法](https://blog.csdn.net/donglynn/article/details/55046017)

[VMware配置linux guest中的shared folder](http://notes.maxwi.com/2017/03/02/vmware-shared-folder/)