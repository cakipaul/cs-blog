---
title: MySQL8.0常用操作与问题解决
date: 2018-12-24 18:00:00
tags:
- mysql
- wordpress
categories:
- 数据库
---
# MySQL正常途径修改密码
参考： https://blog.csdn.net/magicbreaker/article/details/2392764 

其设置密码有以下方法:
```sql
/*这个命令是在/usr/local/mysql/bin中外部命令*/
./mysqladmin -u root -p oldpassword newpasswd;

/*此为第一次设置密码*/
ALTER USER 'root'@'localhost' IDENTIFIED BY 'new password';

/*以下两种需登陆后操作*/
SET PASSWORD FOR root=PASSWORD(’new password’);

UPDATE user SET password=PASSWORD(”new password”) WHERE user=’root’;
```
**注意**：新版的mysql数据库下的user表中已经没有Password字段了，而是将加密后的用户密码存储于authentication_string字段。修改时应为：
```sql
update mysql.user set authentication_string=password('new password') where user='root' and Host = 'localhost';
```
修改后记得刷新权限：
```sql
flush privileges;
```

# Windows下MySQL没有data文件夹
参考：http://www.cnblogs.com/1693977889zz/archive/2018/04/13/8823349.html (MySQL Server [?.?]下会出现data文件夹)（如果要重装）

1. 删除data文件。我的：C:\Program Files\MySQL\MySQL Server 8.0\data

	*注意：这个文件可能在你一直试图操作之后，出现被占用，无法删除的情况，还找不到到底谁占用了。可以尝试重启计算机。*
2. dos下，进入bin目录我的：C:\Program Files\MySQL\MySQL Server 5.7\bin
3. 执行：mysqld –initialize –console 进行初始化（注意要用管理员权限）（此时mysql会自动帮你重新创建data文件夹）
4. 重新打开dos，执行：`net start mysql`

# net start mysql出问题
参考：https://blog.csdn.net/u014266077/article/details/70324724

切换到你的mysql的安装目录下的bin目录，执行`mysqld.exe  –install`命令，看到`service successfully installed`表示安装服务成功。

进到Windows的服务列表，果然MySQL服务出现了，启动mysql服务，然后等待。

# my.ini丢失
新建my.ini文件，将下面复制进去，并放到C:\Program Files\MySQL\MySQL Server 8.0中：（注意路径的修改）
```
[mysql]
# 设置mysql客户端默认字符集
default-character-set=utf8
[mysqld]
#设置3306端口
port = 3306
# 设置mysql的安装目录
basedir=C:/Program Files/MySQL/MySQL Server 8.0/
# 设置mysql数据库的数据的存放目录
#下行保持注释
#datadir=C:/Program Files/MySQL/MySQL Server 8.0/Data
# 允许最大连接数
max_connections=200
# 服务端使用的字符集默认为8比特编码的latin1字符集
character-set-server=utf8
# 创建新表时将使用的默认存储引擎
default-storage-engine=INNODB
```

# mysql-win忘记密码
使用命令：
```bash
mysqld --console --skip-grant-tables --shared-memory
```
开启另一个cmd，输入:
```bash
mysql -u root
```
然后执行sql命令将root用户密码设置为空：
```sql
UPDATE mysql.user SET authentication_string='' WHERE user='root' and host='localhost';
```
查看表：
```sql
select host,user,plugin,authentication_string from mysql.user;
```
mysql8.04之前，MySQL的密码认证插件是“mysql_native_password”，而现在使用的是“caching_sha2_password”.关闭该命令窗，回到开始命令窗，ctrl+c结束，再用`mysql -u root -p`登陆，不需要密码便可进入。

设置新密码：
```sql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'new password';
```
再次登录成功。修改监听IP
Mysql默认监听127.0.0.1（8.04版本为0.0.0.0）的3306端口，如果需要改变可以修改my.cnf的bind-address
添加文件：/etc/my.cnf，内容如下：
```
# Default Homebrew MySQL server config
[mysqld]
# Only allow connections from localhost
#bind-address = 0.0.0.0 #0.0.0.0代表接受所地址的访问
bind-address = 127.0.0.1 #只接受来自本机的访问

# Default Homebrew MySQL server config
[mysqld]
# Only allow connections from localhost
#bind-address = 0.0.0.0 #0.0.0.0代表接受所地址的访问
bind-address = 127.0.0.1#只接受来自本机的访问

# Default Homebrew MySQL server config
[mysqld]
# Only allow connections from localhost
#bind-address = 0.0.0.0 #0.0.0.0代表接受所地址的访问
bind-address = 127.0.0.1 #只接受来自本机的访问
```

# phpMyAdmin登陆MySQL8.0 问题


错误提示：
```
mysqli_real_connect(): The server requested authentication method unknown to the client [caching_sha2_password]mysqli_real_connect(): (HY000/2054): The server requested authentication method unknown to the client 
```

参考：  https://stackoverflow.com/questions/49948350/phpmyadmin-on-mysql-8-0 

原因是新的 caching_sha2_password加密方式还没被php支持，应使用mysql_native_password加密：
```sql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
```
若还不行就需要重装一遍data文件夹（参考上文data文件夹缺失），重新initialize一下。

# CentOS7安装mysql提示“No package mysql-server available
原因是：CentOS7带有MariaDB而不是MySQL，MariaDB和MySQL一样也是开源数据库，可以使用yum命令安装：
```bash
yum -y install mariadb-server
service mariadb restart

#修改密码：
/usr/bin/mysqladmin -u root password 'Password'

#设置开机自动启动:
chkconfig mariadb on
```
如果必须要安装MySQL，首先必须添加mysql社区repo通过输入命令：
```bash
sudo rpm -Uvh http://dev.mysql.com/get/mysql-community-release-el7-5.noarch.rpm
```
最后使用像安装MySQL的常规方法一样安装mysql：
```bash
yum install mysql mysql-server mysql-libs mysql-server
```