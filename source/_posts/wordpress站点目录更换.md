---
title: wordpress站点目录更换
date: 2019-01-10 18:03:55
tags:
- wordpress
categories:
- 前端
---
# 文件搬运

1. 如果数据库和URL保持不变，你只需复制你的文件和数据库即可。
2. 如果数据库和用户名发生变化，编辑 wp-config.php ，确保已将它设置为正确的属性值。如果你想在转移之前测试一下， 你必须临时改变一下数据库表项"wp_options"中的"siteurl" 和 "home"值(可以通过类似phpMyAdmin的工具修改)。
3. 如果你设置了任何的rewrites (永久链接)，你需要禁用 .htaccess ， 成功设置后再重新配置永久链接。

对于单纯的搬运文件夹，还需要删除根目录下的/wp-content/cache文件夹，否则可能出现http error 500 错误。

# 服务器设置
本站使用nginx服务器，编辑config文件：
```bash
vim /etc/nginx/nginx.conf
```
修改其中server里的root文件路径。

# 参考
[wordpress博客搬家](https://codex.wordpress.org/zh-cn:WordPress%E5%8D%9A%E5%AE%A2%E6%90%AC%E5%AE%B6)
