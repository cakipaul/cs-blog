---
title: 使用FontAwesome与Markdown添加小图标
date: 2019-1-2 16:58:00
tags:
- fontawesome
- markdown
categories: 
- 前端
---

<link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/5.6.3/css/font-awesome.css">

<script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js"></script>

## 什么是Font Awesome？

Font Awesome 提供的免费图标字体库与CSS框架，可以利用它展示出精美的矢量图标。例如：

`<i class="fab fa-qq"></i>`渲染后会显示为：

<i class="fab fa-qq"></i>


## 如何使用？
进入[官网](https://fontawesome.com/start)
获取代码，放在网页html头中。

也可以使用cdn，如[BootstrapCDN/fontawesome](https://www.bootstrapcdn.com/fontawesome/)或[staticfile.org](https://www.staticfile.org/)：

```html
<link rel="stylesheet" href="https://cdn.staticfile.org/font-awesome/5.6.3/css/font-awesome.css">
```
或者使用js：
```js
<script defer src="https://use.fontawesome.com/releases/v5.6.3/js/all.js"></script>
```

*注：使用cdn时注意版本号，可以在fontawesome官网查看。截至2019.1.2最新版为5.6.3*

如果您使用的是支持html语法的mark down编辑器，可以将上面的代码粘贴在任意合适的位置（一般放在文章开头或结尾）即可，编辑器会将其自动解析，不会展示出来。

下面是一些图标示例：
```html
<i class="fas fa-igloo"></i> <!-- this icon's 1) style prefix == fas and 2) icon name == igloo -->
<i class="fas fa-igloo"></i> <!-- using an <i> element to reference the icon -->
<span class="fas fa-igloo"></span> <!-- using a <span> element to reference the icon -->
```
经浏览器处理后显示为：
<i class="fas fa-igloo"></i>
<i class="fas fa-igloo"></i>
<span class="fas fa-igloo"></span> 

---
更多示例：

Style|Availability|Style Prefix|Example|Rendering
:-:|:-:|:-:|:-:|:-:|:-:
Solid|Free|fas|`<i class="fas fa-igloo"></i>`|<i class="fas fa-igloo"></i>
Regular|Pro Required|far|`<i class="far fa-igloo"></i>`|<i class="far fa-igloo"></i>
Light|Pro Required|fal|`<i class="fal fa-igloo"></i>`|<i class="fal fa-igloo"></i>
Brands|Free|fab|`<i class="fab fa-font-awesome"></i>`|<i class="fab fa-font-awesome"></i>

*注：其中Regular和Light需要Pro账户登陆，要在官网注册账户并购买，否则无法正常显示。*

更多logo标签可在[IconsGallery](https://fontawesome.com/icons?d=gallery)与[icon列表](https://fontawesome.com/cheatsheet#use)中查看

## 参考
[font awesome免费版官方使用说明](https://fontawesome.com/start)
[fontawesome中文网](http://www.fontawesome.com.cn/get-started/)
[Runoob.com教程](http://www.runoob.com/font-awesome/fontawesome-tutorial.html)