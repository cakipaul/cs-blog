---
title: 使用Gitlab集成发布Hexo+Gitbook
date: 2019-01-21 17:10:00
tags:
- hexo
- gitbook
- gitlab
categories:
- 其他
---
# 1 GitLab Pages
## 1.1 CI/CD介绍
CI/CD是Continuous Integration（持续集成）/Continuous Deployment（持续交付）的缩写。提到CI/CD就不得不说一下Docker——身为Go语言的杀手级应用，Docker通过在一台服务器上运行多个镜像环境，可以充分利用系统的性能，并可以使用各种预先打包的镜像以节约大量时间与带宽。若想在GitLab上使用CI/CD功能，只需要在项目的根目录添加 `.gitlab-ci.yml` 配置文件即可。yml格式的文件可以方便的编辑配置信息，定义集成环境、缓存文件、运行的脚本等。
Docker示意图：

## 1.2 配置.gitlab-ci.yml文件
### 1.2.1 简介
下面的 `.gitlab-ci.yml` 文件已经测试，在每次push代码后，CI/CD都会自动运行，并将静态页面发布到项目Pages上。注意GitLab的Pages服务并不是从某个分支中添加html文件；GitLab的Pages服务直接由CI/CD功能实现，即在用户更新代码后，便按照 `.gitlab-ci.yml` 文件调用服务器、配置环境、运行脚本并发布到pages页面上。所以整个过程便自动实现了每次手动部署的繁琐过程，实现了“手动push，自动deploy”。下面是 `.gitlab-ci.yml` 文件参考：

### 1.2.2 Hexo

```yml
# requiring the environment of NodeJS 10
image: node:10

# add 'node_modules' to cache for speeding up builds
cache:
  paths:
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install hexo-cli -g # install gitbook
  - npm install
  #- npm install hexo-deployer-git --save
  - npm install hexo-filter-mermaid-diagrams --save
  - npm install hexo-generator-searchdb --save

# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - hexo generate
    #- hexo deploy # build to public path
  artifacts:
    paths:
      - public
    #expire_in: 1 week
  only:
    - master # this job will affect only the 'master' branch
```

### 1.2.3 GitBook
```yml
# requiring the environment of NodeJS 10
image: node:10

# add 'node_modules' to cache for speeding up builds
cache:
  paths:
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install gitbook-cli -g # install gitbook
  - gitbook fetch 3.2.3 # fetch final stable version
  - gitbook install # add any requested plugins in book.json

test:
  stage: test
  script:
    - gitbook build . public # build to public path
  only:
    - branches # this job will affect every branch except 'master'
  except:
    - master
    
# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - gitbook build . public # build to public path
  artifacts:
    paths:
      - public
    #expire_in: 1 week
  only:
    - master # this job will affect only the 'master' branch
```


# 2 绑定域名

## 2.1 申请FreeSSL
- 在线申请：[FreeSSL](https://freessl.cn/)
- 下载软件（推荐）：[KeyManager](https://keymanager.org/)

>注：使用TrustAsia时每个二级域名最多申请20个免费证书。

我们可以使用自己的cert.csr（证书请求文件）去数字证书颁发机构（即CA）申请证书，也可以使用它默认提供的随机csr。CA会给提供新的文件cacert.pem，即我们网站要用的数字证书，包括privkey.pem和cacert.pem。

申请验证时需要按提示在DNS解析处添加TXT记录，一般格式为：

From|DNS Record|To
--|--|--
_dnsauth.subDomainName|TXT|\<记录值\>

## 2.2 在Pages页面添加解析
在项目的 `设置-Pages` 中添加域名解析，选择强制https。将对应域名的ssl证书，即pem文件填写在提示位置，并在域名DNS解析处添加解析（按照提示操作即可）：

1. 直接从二级域名开始解析：

From|DNS Record|To
--|--|--
domain.com|A|35.185.44.232
domain.com|TXT|gitlab-pages-verification-code=00112233445566778899aabbccddeeff

2. 从三级域名开始解析：

From|DNS Record|To
--|--|--
subdomain.domain.com|CNAME|namespace.gitlab.io
_gitlab-pages-verification-code.subdomain.domain.com|TXT|gitlab-pages-verification-code=00112233445566778899aabbccddeeff


# 3 从Coding上搬运repo
## 3.1 GitBook
GitBook的部署较为简单，在repo中添加并编辑 `SUMMARY.md` , `README.md` 文件，并按照相应的文件组织格式建立文件夹即可。然后按照上文的建议添加 `.gitlab-ci.yml` 文件，GitLab就会自动集成并部署Pages页面了。

这里提供一个从coding到gitlab批量移动gitbook项目的脚本：
```bash
echo "Type repo's name:"
read repo
cd ./gb/$repo
git remote add gitlab git@gitlab.com:cakipaul/$repo

git checkout --orphan newbook
git clean -df

echo "# requiring the environment of NodeJS 10
image: node:10

# add 'node_modules' to cache for speeding up builds
cache:
  paths:
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install gitbook-cli -g # install gitbook
  - gitbook fetch 3.2.3 # fetch final stable version
  - gitbook install # add any requested plugins in book.json

test:
  stage: test
  script:
    - gitbook build . public # build to public path
  only:
    - branches # this job will affect every branch except 'master'
  except:
    - master
    
# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - gitbook build . public # build to public path
  artifacts:
    paths:
      - public
    #expire_in: 1 week
  only:
    - master # this job will affect only the 'master' branch
" > .gitlab-ci.yml
echo "*~
_book
node_modules
.DS_Store">.gitignore

#清空历史版本
git checkout --orphan newbook
git clean -df
git add -A
git commit -m "reset repo"
git branch -D master
git branch -m master
git push -f coding master
git push -f gitlab master
```

## 3.2 Hexo
我们首先fork GitLab上的Pages项目：[GitLab Pages-hexo](https://gitlab.com/pages/hexo)，在 `设置-高级` 中取消fork关系，然后克隆到本地进行操作：

```bash
# 1 克隆项目到本地
git clone https://gitlab.com/userName/repoName

# 2 克隆主题
#next 6.0+
git clone https://github.com/theme-next/hexo-theme-next themes/next
#material
git clone git clone https://github.com/viosey/hexo-theme-material.git themes/material
#若使用自定义主题，就可以删除原有的 themes/landscape 文件夹

# 3 克隆coding（或其他git库）上原有项目到old-version文件夹
git clone https://dev.tencent.com/userName/repoName origin-version

# 4 移动原有的源文件到根目录并覆盖原有文件，注意要先删除.git文件夹
rm -rf ./old-version/.git
\cp -rf ./old-version/* ./
#注意 cp 命令前加了 \ ，意思是不让它用~/.bashrc里的别名运行，即避免 -i 参数

# 5 将上文中的 .gitlab-ci.yml 文件复制在文件夹下

# 6 git操作
git add .
git commit -m "update"
git push -u origin master
```
这样，GitLab就会自动部署到Pages页面了，通过 userName.gitlab.io/repo/即可访问
>提示：
>- hexo的 _config.yml 文件中的“root: /”部分需要根据自己的域名进行设置。如果配置错误将会导致网站只能显示html首页，且css样式渲染失败；
>- 一些主题内的.git文件夹可以删除。另外特别的，material主题中有单独的.gitignore文件，会忽略掉该主题的 _config.yml 文件，要注意删除。

---
参考：
- [Configuration of your jobs with .gitlab-ci.yml](https://gitlab.com/help/ci/yaml/README.md)
- [持续集成在Hexo自动化部署上的实践](https://qinyuanpei.github.io/posts/3521618732/)

