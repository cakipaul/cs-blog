---
title: 使用Pandoc实现文档格式转换
date: 2019-6-22 05:00:00
tags:
- pandoc
categories:
- 其他
---

# 文档格式问题？使用 Pandoc！

Pandoc是由John MacFarlane开发的标记语言转换工具，可实现不同标记语言间的格式转换，堪称该领域中的“瑞士军刀”。

Pandoc可以在以下格式之间转换：

- 轻量级标记格式
- HTML格式
- 电子书
- 文档格式
- TeX格式
- XML格式
- 交互式笔记本格式
- 字处理器格式
- 页面布局格式
- 大纲格式
- Wiki标记格式
- 幻灯片放映格式
- 自定义格式（自定义编写器可以用lua编写）
- PDF

# 安装 Pandoc 并使用它！
在 GitHub 上下载最新 Release：https://github.com/jgm/pandoc/releases/

我们还可以使用在线演示版本： https://pandoc.org/try/

Pandoc是一个命令行工具，没有图形用户界面。因此，要使用它需要打开一个终端窗口。在下载安装后，让我们验证是否安装了pandoc：

```
pandoc --version
```

以将 .md 文件转换为 HTML 文件为例，可使用以下命令：

```
pandoc test1.md -f markdown -t html -s -o test1.html
```

其中：

- 文件名 `test1.md` 告诉 pandoc 要转换的文件名
- `-s` 表示创建一个“独立”文件，带有页眉和页脚，而不仅仅是一个片段
- `-o test1.html` 表示将输出放在 test1.html 中。
- 注：可以省略 `-f markdown` 和 `-t html` ，因为默认是从降价转换为 HTML

更多参考官网文档：https://pandoc.org/MANUAL.html#