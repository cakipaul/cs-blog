---
title: 使用SHELL脚本批量部署gitbook页面
date: 2019.1-12 16:17:00
tags:
- gitbook
- shell
categories:
- 前端
---

# 1 思路
## 1.1 概述
在使用Coding平台的Pages服务发布gitbook时，我们可以在master分支上部署源代码，coding-pages分支上部署静态页面。将项目命名为有规律的单词拼写，就可以使用一个脚本对不同的仓库进行初始化/更新文件/更新页面操作了。


## 1.2 使用脚本初始化仓库

在初始化仓库时需要设置book.json、SUMMARY.md文件，前者包含所需要的插件与站点信息，后者则包含目录信息。初始化（即gitbook init & gitbook install）后会生成插件文件夹node_modules，其中包含book.json中所涉及的插件。

```mermaid
sequenceDiagram
participant l as local
participant r as remote
    l-->>l:获取init所需要的必要信息
    note right of l:local git init
    r->>l:master:pull
    note right of l:gitbook build
    l-->>l:coding-pages
    note right of l:将_book文件夹置为根目录
    l->>r:coding-pages:push-f
    l->>r:master:push(含book.json、SUMMARY.md)
```


## 1.3 使用脚本更新新文件
在涉及文件改动，即SUMMARY.md文件有改动时,local需要先清理本地文件再pull拉取远程仓库的master中的源文件，流程如下：

```mermaid
sequenceDiagram;
participant l as local;
participant r as remote;
    l-->>l:init
    note right of l:local git init
    r->>l:master:pull
    note right of l:gitbook build
    l-->>l:coding-pages
    note right of l:将_book文件夹置为根目录
    l->>r:coding-pages:push-f
    l->>r:master:push(由SUMMARY.md生成的新目录)
```

## 1.4 使用脚本更新页面
更新页面操作不会影响remote仓库中的master分支，只会拉取源文件并生成静态页面，再push到coding-pages分支上。流程如下：

```mermaid
sequenceDiagram;
participant l as local;
participant r as remote;
    r->>l:master:pull;
    note right of l:gitbook build;
    l-->>l:coding-pages;
    note right of l:将_book文件夹置为根目录;
    l->>r:coding-pages:push-f;
```


# 2 实现
## 2.1 文件组织
以/home中实现gitbook部署环境为例：
```
/home/deploy.sh
     /node_modules/...
     /gb/db/SUMMARY.md
           /README.md
           /book.json
           /floders...
        /ds/SUMMARY.md
           /README.md
           /book.json
           /floders...
        /...

```
其中：
- deploy.sh ：即脚本文件；
- node_modules/ ：是为了重复利用`gitbook install`的插件而生成的插件目录；
- gb/ ：即新生成的gitbook数据所在的根目录；
- db/ ds/ .../ ：电子书目录，db为database的缩写，ds为data structure的缩写。
在/home/目录中编辑deploy.sh文件，就可以通过 `sh deploy.sh` 命令来生成这个目录树了。

## 2.2 流程
通过vim命令自定义编辑deploy.sh文件后，运行`sh deploy.sh`进入部署流程：

```mermaid
graph TD;
A(仓库确认:输入仓库名称)-->B(在./gb/目录下生成同名文件夹);
B-->C{Quick deploy?};
C-->|y|D(从远程仓库的master分支pull文件<br/>生成静态页面后推送到coding-pages分支);
D-->H(End);
C-->|n|E{init?};
E-->|n|G(重置git仓库,更新页面);
G-->H;
E-->|y|F(配置book.json与SUMMARY文件);
F-->I(初始化git仓库,更新页面);
I-->H;
```

*注意：y/n皆为小写*

init流程耗时较多，一般只需要在初次部署时运行，之后若有文件组织的改动，则两i选择都选 'n' 。在涉及文件内容的编辑时，在Quick deploy时键入y即可。

## 2.3 deploy.sh文件代码
库初始化时添加 gitignore、book.json与SUMMARY.md文件。

其中.gitignore内容为：
```
node_modules
deploy.sh
_book
```

在建立gitbook仓库的根目录中vim编辑deploy.sh文件，为deploy.sh脚本授权：
```bash
chmod +x ./deploy.sh
```
之后运行即可。

deploy脚本代码如下：
```bash
#!bin/sh

#需要修改为自己的coding账户相对应的url格式
echo "type the repo's name (using git@git.dev.tencent.com:paulsun/)"
read book
coding="git@git.dev.tencent.com:paulsun/$book"

echo "Quick(not the first) deploy?(y/n)?"
read quick

if [[ $quick == 'n' ]]
then
    mkdir ./gb
    mkdir ./gb/$book
    find ./gb/$book/ |grep -v ./gb/$book/deploy.sh|grep -v ./gb/$book/node_modules|grep -v ./gb/$book/|xargs rm -rf
    rm -rf ./gb/$book/.git

    #init node_moules&book.json&SUMMARY
    echo "Do you want to start init progress?(y/n)"
    read init
else
    init="n"
fi

#init-deploy
if [[ $init == "y" ]]
then

#此处为book.json代码，包含gitbook 3.2.3版本推荐插件。注意修改为个人信息

    author="cakipaul"
    links="http://cs-book.cakipaul.com"
    flag="false" #show-level
    echo "title="
    read title
    echo "description="
    read description
    echo  "{
    \"title\": \"$title\",
    \"author\": \"$author\",
    \"description\": \"$description\",
    \"output.name\": \"site\",
    \"language\": \"zh-hans\",
    \"gitbook\": \"3.2.3\",
    \"links\": {
        \"sidebar\": {
            \"Home\": \"$links\"
        }
    },
    \"plugins\": [
        \"anchors\",
        \"ace@^0.3.2\",
        \"anchor-navigation-ex@0.1.8\",
        \"-sharing\",
        \"sharing-plus\",
        \"mermaid-gb3\",
        \"katex\",
        \"-lunr\",
        \"-search\",
        \"search-plus\",
        \"-highlight\",
        \"page-footer-ex\",
        \"prism\",
        \"prism-themes\",
        \"splitter\",
        \"page-footer-ex\"
    ],
\"pluginsConfig\": {
        \"theme-default\": {
            \"showLevel\": $flag
        },
        \"sharing\": {
            \"douban\": false,
            \"facebook\": false,
            \"google\": false,
            \"hatenaBookmark\": false,
            \"instapaper\": false,
            \"line\": false,
            \"linkedin\": false,
            \"messenger\": false,
            \"pocket\": false,
            \"qq\": true,
            \"qzone\": true,
            \"stumbleupon\": false,
            \"twitter\": false,
            \"viber\": false,
            \"vk\": false,
            \"weibo\": true,
            \"whatsapp\": false,
            \"all\": [
                \"facebook\",
                \"google\",
                \"twitter\",
                \"linkedin\",
                \"douban\"
            ]
        },
        \"prism\": {
            \"css\": [
            \"prism-themes/themes/prism-base16-ateliersulphurpool.light.css\"
            ]
        },
        \"anchor-navigation-ex\": {
            \"isRewritePageTitle\": true,
            \"isShowTocTitleIcon\": true,
            \"tocLevel1Icon\": \"fa fa-hand-o-right\",
            \"tocLevel2Icon\": \"fa fa-hand-o-right\",
            \"tocLevel3Icon\": \"fa fa-hand-o-right\"
        },
        \"page-footer-ex\": {
            \"copyright\": \"By [$author]($links)，使用[知识共享 署名-相同方式共享 4.0协议](https://creativecommons.org/licenses/by-sa/4.0/)发布\",
            \"markdown\": true,
            \"update_label\": \"此页面修订于：\",
            \"update_format\": \"YYYY-MM-DD HH:mm:ss\"
        }
    }
}" >./gb/$book/book.json
    gitbook install
    cp  -r ./node_modules ./gb/$book/

    echo "# Summary
* [本书首页](README.md)

### 参考资源
* [分类链接](参考资源/分类链接.md)" >./gb/$book/SUMMARY.md
fi

cd ./gb/$book

#noquick-deploy
if [[ $quick == "n" ]]
then
    git init
    echo "node_modules
deploy.sh
_book">.gitignore
    git add .
    git commit -m '_book'
    git remote rm coding
    git remote add coding $coding

    git checkout master
    git pull coding  master

    gitbook init
    gitbook install
    gitbook build
    git add .
    git commit -m "create files"

    git checkout --orphan coding-pages
    git checkout coding-pages
fi

#quick-delpoy 
if [[ $quick = 'y' ]]
then
    git checkout master
    git pull coding master
    gitbook build

    git checkout coding-pages
elif [[ $quick != 'n' ]]
then
    echo "Illegal input"
    exit
fi

git rm --cached -r .
git clean -df
rm -rf *~
cp -r _book/* .
echo "node_modules
deploy.sh
_book">.gitignore
git add .
git commit -m "_book"
git push -u coding coding-pages -f

git checkout master

if [[ $quick == 'n' ]]
then
git push -u coding master -f
fi

```

