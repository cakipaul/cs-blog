---
title: 使用coding的Pages服务发布Gitbook
date: 2019-1-11 16:58:00
tags:
- coding
- gitbook
- pages
categories: 
- 前端
---
# 1 使用Coding
## 1.1 添加ssh
```bash
#生成密钥
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
#生成过程需要按三次回车键，即按照默认设置生成即可

#Linux上查看生成的密钥
cat ~/.ssh/id_rsa.pub
#或者
vim ~/.ssh/id_rsa.pub

#win下查看密钥
#打开用户文件夹下的.ssh/文件夹，里面有id_rsa.pub文件。
```
在coding个人设置中，选择“ssh公钥”，新建条目，将id_rsa.pub中的内容复制进去，选择有效时间后保存即可。

## 1.2 初始化仓库
coding使用ssh连接的url格式为：

`git@git.dev.tencent:yourName/yourRepo`

使用git命令将本地文件添加到远程仓库：
```bash
#初始化git
git init

#添加当前目录文件
git add .

#commit
git commit -m "first commit"

#添加远程仓库
git remote add coding git@git.dev.tencent.com:yourName/yourRepo

#上传至仓库
git push -u coding master
```
ssh连接coding可能需要先进行验证，否则会提示could not readfrom remote repo的错误：
```bash
ssh -T git@git.dev.tencent.com
```
# 2 使用Gitbook
可以参考：

[Git-Node.js-Hexo-Gitbook环境搭建与使用指南](https://blog.cakipaul.com/2019/01/07/Git-Node.js-Hexo-Gitbook%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA%E4%B8%8E%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97/)

[Gitbook命令行速览](https://tonydeng.github.io/gitbook-zh/gitbook-howtouse/howtouse/gitbookcli.htm)

```bash
#将gitbook部署到/home/目录
cd /home/

#将gitbook文件克隆到本地
git clone https://github.com/yourName/yourRepo
cd gitbook

#初始化/安装插件/生成静态页面(_book文件夹)
gitbook init
gitbook install
gitbook build
cd _book

#使用git命令将静态页面提交到coding的coding-pages分支
git init
git add .
git commit -m "2019.1.11"
git checkout -b coding-pages
git remote add coding git@git.dev.tencent:yourName/yourRepo
git push -u coding coding-pages
```

# 3 部署Pages
静态 Coding Pages 允许的部署分支来源为`master`分支和`coding-pages`分支，默认部署来源是master 分支，用户可在设置里更改部署来源。部署成功后后可通过

`<user_name>.coding.me/<project_name>`

的域名的 URL 访问静态 Pages。

**部署步骤：**
1. 在 Coding.net 中创建一个项目：

    推荐创建的项目名为 {username}.coding.me， 
这样稍后生成的 Pages 默认链接即为 {username}.coding.me；
2. 在仓库中新建 index.html 文件，作为默认网页入口；
3. 通过仓库中的“Pages 服务”菜单进入设置页面， 在部署来源中选择"coding-pages"分支；
4. 在Pages设置的CNAME选项中绑定域名，如本页面的book.cakipaul.com。

# 4 参考

[Generating a new SSH key and adding it to the ssh-agent](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)

[Coding Pages](https://dev.tencent.com/help/doc/creating-pages)