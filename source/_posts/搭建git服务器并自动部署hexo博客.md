---
title: 搭建git服务器并自动部署hexo博客
date: 2020-1-10 14:52:00
tags:
- git
- hexo
- shell
categories:
- 其他
---

# 搭建 git 服务器
## 安装Git

```bash
yum install curl-devel expat-devel gettext-devel openssl-devel zlib-devel perl-devel
yum install git
```

创建一个git用户组和用户，用来运行git服务：

```bash
groupadd git
useradd git -g git

#设置用户密码
passwd git
```
## 创建证书登录

收集所有需要登录的用户的公钥，公钥位于 `id_rsa.pub` 文件中，把我们的公钥导入到 `/home/git/.ssh/authorized_keys` 文件里，一行一个。 

如果没有该文件创建它：

```bash
cd /home/git/
mkdir .ssh
chmod 755 .ssh
touch .ssh/authorized_keys
chmod 644 .ssh/authorized_keys
```

在 /home/git/.ssh/authorized_keys 文件中添加 id_rsa.pub d的内容即可。

* windows ： C:\Users\<username>\.ssh\id_rsa.pub
* linux ： /root/.ssh/id_rsa.pub 

如果没有，则使用 ssh-keygen -t rsa -C "cakipaul@gmail.com"。注意要先 su git 切换到 git 用户。

也可以命令行直接导入证书：

```bash
cd C:\Users\用户ssh git@git.cakipaul.com 'cat >> .ssh/authorized_keys' < ~/.ssh/id_rsa.pub

ssh-copy-id -i ~/.ssh/id_rsa.pub git@git.cakipaul.com
```

## Git 服务器打开 rsa 认证

打开配置文件：

```bash
vim /etc/ssh/sshd_config
```

将三个配置项置为 yes：

```bash
RSAAuthentication yes
PubkeyAuthentication yes
AuthorizedKeysFile .ssh/authorized_keys
```

重启sshd：

```bash
systemctl stop sshd.service
systemctl start sshd.service
```

## 初始化Git仓库

首先我们选定一个目录作为Git仓库，假定是 `/home/gitrepo/blog.git` ：

```bash
cd /home
mkdir gitrepo
chown git:git gitrepo/
cd gitrepo
git init --bare blog.git
```

以上命令Git创建一个空仓库，服务器上的Git仓库通常都以.git结尾。然后，把仓库所属用户改为git：

```bash
chown -R git:git blog.git
```

## push 仓库到服务器

```bash
git remote add cvm git@git.cakipaul.com:/home/gitrepo/blog.git
git push -u cvm master
```

# 配置 hooks

需求：在 pc 上 push 更新到服务器，服务器将更新自动部署到网站。

```bash
cd /home
#创建工作区
git clone git@localhost:/home/gitrepo/blog.git
#给git用户权限
sudo chown git:git -R /home/blog
#编辑hook
vim /home/gitrepo/blog.git/hooks/post-receive
```

hook 参考正文：

```bash
#!/bin/sh
while read oldrev newrev ref; do
    if [[ $ref =~ .*/master$ ]]; then
        echo "Master ref received.  Deploying master branch to production..."
        # git --work-tree=/home/blog --git-dir=/home/gitrepo/blog.git checkout -f
        DEPLOY_PATH=/home/blog
        # git的hooks里面默认有一些环境变量,会导致无论在哪个语句之后执行git命令都会有一个默认的环境路径，既然这样unset 掉默认的GIT环境变量就可以了。
        unset GIT_DIR #这条命令很重要
        cd $DEPLOY_PATH
        git reset --hard
        git pull origin master
        git push gitlab master
        git push coding master
        export NVM_DIR="$HOME/.nvm"
        [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
        [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
        nvm use stable
        cnpm install hexo-deployer-git --save
        cnpm install hexo-filter-mermaid-diagrams --save
        cnpm install hexo-generator-searchdb --save
        #cnpm install hexo -g --save
        hexo g
        # chown nginx:nginx -R $DEPLOY_PATH/public
    else
        echo "Ref $ref successfully received.  Doing nothing: only the master branch may be deployed on this server."
    fi
done
```

配置执行权限：

```bash
chmod +x /home/gitrepo/blog.git/hooks/post-receive
```

# 后续问题

## 禁止git用户ssh登录

```bash
vim /etc/passwd
```

将 git:x:1004:1005::/home/git:/bin/bash 改为 git:x:1004:1005::/home/git:/bin/git-shellgit 

## 每次仍需要密码登录    

查看安全日志：

```bash
cat /var/log/secure|grep git    
```

发现：Authentication refused: bad ownership or modes for directory /home/git    

修正权限：

```bash
chmod 700 /home/git 
```

(权限千万不能赋予777，否则免密登录是无法生效的，因为authorized_keys默认是自己唯一写权限的)

另外如果日志中出现 user git not allowed beecause account is locked ，则需要解锁一下密码：

```bash
sudo passwd -u git
```

## 报错：remote: error: cannot run hooks/post-receive: No such file or directory    

如果文件存在且已授权，则原因是文件编码应使用 LF换行，而不是 CRLF。在 linux 下直接 touch 生成即可。