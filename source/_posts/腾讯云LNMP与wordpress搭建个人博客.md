---
title: 腾讯云CentOS7.5+LNMP+WordPress5个人博客网站搭建流程
date: 2018-12-22 16:00:00
tags:
- nginx
- wordpress
categories:
- 前端
---

# 1 开始之前

## 1.1 预备知识

### 1.1.1 一个网站运行的基本原理

网站的本质是运行在服务器上可供访问的资源，按存在状态区分，可分为静态资源和动态资源。对用户而言，若想要访问一个网站，只需要在浏览器中输入这个网站的域名，再按回车就可以了。这个回车按下去之后具体发生了什么呢？

在整个互联网上，区分每台服务器身份的是“IP”（Internet Protocol Address，又译为网际协议地址）地址。理论上讲，每台计算机都应该在网络上有唯一的IP地址（实际上对IPv4（v4就是第四代）来说由于地址不够用，有NAT等协议来进行IP复用，若想了解这个技术的详情，需要一定的计算机网络相关知识），这个IP地址是由小区/学校/公司/运营商等本地局域网通向广域网的路由器给你的计算机自动分配的。对服务器来说，它必须拥有一个固定的IP，并且在域名管理服务器（即DNS（Domain Name System,域名服务系统）服务商）上将自己的IP与域名绑定注册，才能让用户通过域名查询到自己的IP地址，进而访问相应资源。

客户端拿着域名这把“钥匙”，通过域名服务器获得了该域名对应的IP后，下一步就是去请求连接这个IP。既然是连接，就要遵守连接原则，就网页技术来说，默认使用的就是http协议（HyperText Transfer Protocol，超文本传输协议）。另一种就是https协议，区别就是https多了一层加密协议，可以更安全的进行资金交易、密文传递，这也是现在企业级网页的主流协议。浏览器默认会在域名之前补全这个协议名，比如说本站可以输入www.cakipaul.com访问，在浏览器地址栏中实际上是 http://www.cakipaul.com 。如果用IP地址表示的话，就是 http://129.28.110.147.
这里还要提到“端口”的概念。每台具有网络功能的计算机（包括服务器）同一时间往往有多个网络程序在运行，像是边听音乐边浏览网页这件事中，就有两个应用程序需要联网获取资源。这样的话，在一根网线上怎么区别两个程序的流量呢？一种方法是让两个程序在自己需要发送的每段信息的开始部分就声明自己的程序名，然后再发送出去；接收数据时，操作系统负责鉴别该信息的归属，并将其分配给相应的应用程序。但是这不仅浪费有效信息、增加延迟，还给了恶意程序可趁之机：他可以把自己模仿成别的程序，窃取或侦听用户敏感数据。于是我们引入了“端口”的概念：每个应用程序在本机IP之上再绑定一个属于自己的唯一端口号，这个端口号可能是为特殊的软件预留的（即约定俗成的），也可能是系统运行时自动分配的。对服务器来说，他的端口号应该使用约定默认的端口，以http传输数据为例，默认的端口号就是80。所以在访问网页时，浏览器不仅补全了http://协议字段，还补全了:80端口字段。端口字段如果是80的话，在浏览器中默认不会显示，但是如果输入www.cakipaul.com:80也是没有问题的。如果服务器http服务不使用80端口，比如说使用8080端口，那么端口字段就必须明文打出来了：www.cakipaul.com:8080(这里只是用作示例，本站实际上8080端口并不提供服务)。

服务器实际上一直在“监听”自己的80端口的，一旦收到用户端的连接请求，就会发起建立连接过程，即基于“TCP”（Transmission Control Protocol，传输控制协议）协议的安全“会话”。如果客户端是直接输入域名访问的，服务端会检查自己网页资源默认目录下的默认文件，一般来说就是index（.html/.htm/.php等后缀名）文件。然后服务器会将文件直接发送/解读后发送，客户端的浏览器就会自动分析收到的代码/文件，然后排版展示给用户了。

总结一下访问网页时浏览器与本地计算机的幕后工作流程（并不完整，在此只突出了核心流程）：
用户输入域名/点击链接→服务器分析并补全域名（http://协议字段与:80端口字段，如果用户端已经声明了协议或端口就不必再执行补全操作）→如果本地（hosts文件，不同操作系统下该文件的位置不同，可以进行自主配置该文件以节省访问DNS服务器的频率、加快域名解析）有域名和IP地址的映射就直接访问该IP地址，否则就查询域名服务器（DNS）取得IP→通过IP+端口（默认http连接是80端口）向服务器发起请求→服务器处理该请求并返回相应数据→客户端浏览器解析数据并显示。

### 1.1.2 我们要做什么样的网页

我们希望做一个具有完善的数据管理、发布与更新功能、插件拓展功能、排版美化功能以及良好安全性与响应速度的网站。
这需要：

* 域名：（需要实名注册）将域名与运行网页的服务器所在的IP绑定后，便可以通过域名访问自己的网页了；
* 服务器计算机与它的公网IP：提供软件运行的硬件环境与网络环境；
* 服务器端运行的软件：
	* 操作系统：负责接收远程登录请求、操作本地计算机文件、运行相应软件并控制权限与安全。可以是Windows系统 ，也可以是更常用的Linux系统（Linux占用的系统性能更低，而且Linux是开源软件，不需要额外费用，安全性也有所有开源技术人员的保障）
	* 数据库管理软件：管理数据库并对访问数据库的用户（或其他软件）进行读写操作的权限管理，保障数据的安全性，并具有新建、查找、修改、删除、备份等功能；
	* 服务器软件：监听端口（默认80）以接受客户端连接请求，处理用户数据并返回合适的资源；
	* 后端脚本处理软件：所谓脚本，就是语法要求宽松、功能整合度高的代码，其存在的意义就是加快开发速度、降低开发复杂度。脚本处理软件通常与服务器软件合作，后者负责监听端口、取得客户端请求并交给脚本软件分析，脚本软件分析脚本代码、处理好后将返回信息交给后者，服务器软件再把返回信息传给客户端浏览器；
	* 博客维护软件：这一项并不是必选项，但是它将大大简化开发步骤，节省大量的开发时间。它和后端脚本处理软件类似，并且大多提供图形化的维护界面，让用户可以通过直观的操作来设计博客页面布局、设计相关功能并更新、修改、删除博客文章。

### 1.2.3 为什么是CentOS、LNMP与WordPress

在这里我们选择是：

* CentOS：它是我们云服务器上的操作系统，是Linux（最好的开源操作系统）发行版之一，来自于Red Hat Enterprise Linux依照开放源代码规定发布的源代码所编译而成。有些要求高度稳定性的服务器以CentOS替代商业版的Red Hat Enterprise Linux使用。也可以选择Ubuntu，它是基于Debian GNU和Linux的免费开源桌面PC操作系统，支持x86、amd64(即x64)和ppc架构。它也是目前使用用户最多的Linux版本，用户数已超过十亿（2015，包含手机等设备）。
* LNMP（也有LAMP）：
	* L:Linux，即上文中的CentOS；
	* N:Nginx（engine x，也有人选择使用A：Apache）是一款开源的服务器软件。Nginx采用异步框架（意味着更好的处理并发请求），并能处理负载均衡。该软件由IgorSysoev创建，并于2004年首次公开发布，同名公司成立于2011年，以提供支持。相对于Apache来说，Nginx占用更少的内存及资源，并且有利于高并发环境。整体设计高度模块化，编写模块相对简单、稳定，社区精品模块生产活跃。Apache则是老牌服务器，胜任大多类型的网页需求，并且整体性能稳定、模块更多。Nginx的优势是处理静态请求，CPU内存使用率低，Apache适合处理动态请求，所以现在一般前端用Nginx作为反向代理抗住压力，Apache作为后端处理动态请求。
	* M:MySQL（官方发音为/maɪ ˌɛskjuːˈɛl/，即My-S-Q-L，但也经常读作/maɪ ˈsiːkwəl/:“My Sequel”）数据库管理软件，负责管理本地数据，并分配用户以读写权限。原本是一个开放源代码的关系型数据库（一种数据库规范化标准）管理系统，原开发者为瑞典的MySQL AB公司，2008年被SunMicrosystems收购，该公司再2009年被甲骨文公司收购，现在MySQL为Oracle旗下的产品。它不仅能满足小型网站的需求，许多大型网站（如wikipedia,google,facebook）也开始使用MySQL管理自己的数据库。（注：M也可以是MariaDB，它是MySQL的一个分支。在MySQL被Oracle公司收购后，MySQL就有了闭源的潜在风险，因此社区采用分支的方式来避开这个风险）
	* P:PHP（PHP:Hypertext Preprocessor，即“PHP：超文本预处理器”）是开源的通用计算机脚本语言，尤其适用于网络开发并可嵌入HTML中使用。PHP的语法借鉴吸收C语言、Java和Perl等流行计算机语言的特点，易于一般程序员学习。PHP的主要目标是允许网络开发人员快速编写动态页面，但PHP也被用于其他很多领域。
	* WordPress：WordPress是基于PHP脚本处理和MySQL数据库的开源博客软件和内容管理系统。WordPress内置插件架构和模板系统，可以添加丰富的功能、在已有的主题基础上进行快速再设计。截至2018年12月，排名前1000万的网站超过32.6%使用WordPress 。如今，它不仅可以用来制作博客，还可以制作各种展示/交易/论坛类网站。

## 1.3 材料清单

以本站为例，使用的服务有：
* 域名与服务器：腾讯云平台
* 操作系统：CentOS7.5
* 服务端：Nginx1.12.2
* 数据库：MySQL8.0
* 后端脚本处理：PHP5.6
* 管理平台：WordPress5.1
* 常用插件推荐（见下文）

# 2 购买域名与服务器

## 2.1 域名服务基本原理

### 2.1.1 域名注册与认证

域名本质来讲是一种字段解析服务，域名本身由“.”来分割成数个字段，以www.cakipaul.com为例，其中包含了“www”、“cakipaul”、“com”三个字段，每个字段都有相应的解析服务器，其中最后面的字段所在服务器被称作顶级域名服务器，包括通用顶级域，例如 .com、.net和.org；以及国家和地区顶级域，例如 .cn、.us和.hk。顶级域名下一层是二级域名，一级一级地往下。以在.com域名注册为例，我们需要在.com顶级域名服务器中登记自己的域名，在这里就是“cakipaul.com”。这一工作由云服务商代为操作。
注册好域名之后我们就可以在自己的云平台中拥有一个独一无二的域名资产了，下一步就是实名认证，在云平台进入域名管理页面，选择实名认证，之后按照提示操作便可。

### 2.1.2 购买主机与备案

如果是为了建立个人网页、每日流量不超过几千，购买最低性能的云服务器主机便可以，本站（2018.12）主机配置为：

>操作系统 CentOS 7.5 64位<br/>
>CPU 1 核<br/>
>内存 1 GB<br/>
>公网带宽 1 Mbps

购买云主机后别忘了在云平台更新自己服务器的操作系统密码，一般来说超级管理员（拥有最高读写权限）用户名都是root（这个用户名下面还会多次看到），密码按照个人习惯设定就可以了。

### 2.1.3 解析域名

查看云主机的公网IP（公网是相对于内网/子网而言的，可以理解为前者面向互联网，后者面向局域网），并在自己的域名管理界面，将该IP地址解析到域名之下。一般的云平台都会提供“云解析”功能，或者直接得到自己域名账户下查看，也可以找到和“解析”相关的功能。

可以通过“新手一键解析”功能进行部署，也可以手动添加解析。解析按照不同的记录类型划分为：

* A记录：将域名解析到一台独立的云主机上，其值为IP地址。传回一个 32 比特的 IPv4 地址，最常用于映射主机名称到 IP地址，但也用于DNSBL（RFC 1101）等。
* CNAME记录：将域名解析到另一个域名上，其值为域名。规范名称记录，一个主机名字的别名：域名系统将会继续尝试查找新的名字。
* MX记录：用于邮件交换协议的传递，其值为域名。引导域名到该域名的邮件传输代理（MTA, Message Transfer Agents）列表。
* AAAA记录（目前不常用，未来大势所趋）：Pv6 IP 地址记录，传回一个 128 比特的 IPv6 地址，最常用于映射主机名称到 IP 地址。
* TXT记录：用于从DNS层面返回一个字符串，多用于域名验证等操作。最初是为任意可读的文本 DNS 记录。自1990年起，些记录更经常地带有机读数据，以 RFC 1464 指定：机会性加密（opportunistic encryption）、Sender Policy Framework（虽然这个临时使用的 TXT 记录在 SPF 记录推出后不被推荐）、DomainKeys、DNS-SD等。

对个人主页来说，添加两个A记录即可，一个为“@”（即缺省三级域名）一个为“www”，并将云主机公网IP填写到记录值中。
解析好后还需要网站备案，这个备案和服务器相绑定。以腾讯云为例，你需要拥有至少三个月的云主机时长才可以进行备案，按照备案流程要求与提示填写个人信息，剩下的就是等待。在备案审核通过之前，通过域名访问个人主页可能会跳转到相关备案等待界面。备案审核通过后就可以通过域名访问主页了，记得按照要求在主页下方添加备案号哦~（wordpress设置功能里有该选项）

## 2.2 CentOS（Linux）的基本管理

### 2.2.1 Linux系统简介

一、系统启动过程

可以分为5个阶段：（具体可以参考[ReadMore…]）
1. 内核的引导：
	* 通电后，首先是BIOS（Basic Input/Output System，基本输入输出系统）开机自检，按照BIOS中设置的启动设备（通常是硬盘）来启动；
	* 操作系统接管硬件以后，首先读入 /boot 目录下的内核文件。
2. 运行init：
	* init 进程是Linux系统所有进程的起点，系统中其他任何进程都源于它开始启动。init 程序首先读取配置文件 /etc/inittab；
	* 许多程序需要开机启动，在Windows叫做”服务”（service），在Linux就叫做”守护进程”（daemon）。init进程需要运行这些开机启动的程序，但是，不同的场合需要启动不同的程序。比如我们的服务器需要启动Nginx，但如果是Ubuntu桌面式Linux系统就不需要。为此Linux允许为不同的场合分配不同的开机启动程序，这就叫做”运行级别”（runlevel）。即根据”运行级别”，确定哪些程序会被启动；
	* Linux下有0-6七个运行级别(0和6将导致系统无法启动)。
3. 系统初始化：
    * rc.sysinit是每一个运行级别都要首先运行的重要脚本。它主要完成的工作有：激活交换分区，检查磁盘，加载硬件模块以及其它一些需要优先执行任务。
4. 建立终端：
	* rc执行完毕后，init继续执行。此时系统环境已基本就位，各种守护进程也已经启动。init接下来会打开6个终端，以便用户登录系统：
```bash
1:2345:respawn:/sbin/mingetty tty1
2:2345:respawn:/sbin/mingetty tty2
3:2345:respawn:/sbin/mingetty tty3
4:2345:respawn:/sbin/mingetty tty4
5:2345:respawn:/sbin/mingetty tty5
6:2345:respawn:/sbin/mingetty tty6
```

getty是“get teletype”的缩写,它是一个Unix程序，用来连接物理的或虚拟终端。mingetty即mini-getty，是linux 下精简版的getty，适用于本机上的登入程序。

5. 用户登陆系统：
    * 命令行登录
	* ssh登录
	* 图形界面登录

Linux保存数据、关机与重启：
```bash
sync //将数据由内存同步到硬盘中。不管是重启系统还是关闭系统，一般都要运行该指令
shutdown –[?] [min] //[?]处常用h:关机 r:重启 [min]处填写分钟数或者[hh:mm]格式的时间，也可以是now
halt //关闭系统 等同于shutdown –h now
poweroff //关闭系统 等同于shutdown –h now
init 0 //等同于 shutdown –h now
shutdown –r now //重启
reboot //重启
init 6 //重启
```

云服务器如果关机了，需要通过云平台将其打开，一般情况下云服务器不用关机，只需要使用`reboot/init 6/shutdown-r now`指令来管理云主机。

二、Linux文件目录

最常用的目录：

* /etc 存放系统管理所需要的配置文件和子目录；
* /usr 放置用户的很多应用程序和文件，类似于windows下的program files目录；
* /bin, /sbin, /usr/bin, /usr/sbin 系统预设的执行文件的放置目录，比如 ls 就是在/bin/ls 目录下的。其中/bin, /usr/bin 是给系统用户使用的指令（除root外的通用户），而/sbin, /usr/sbin 则是给root使用的指令；
* /var 存放系统上所运行的每个程序的日志记录，具体在/var/log 目录下。另外mail的预设放置也是在这里。

### 2.2.2 连接Linux

我们可以通过Windows下的CMD命令控制行（通过win+r，输入cmd打开，一般在搜索栏或根目录下通过管理员身份打开）来与服务器建立链接，输入以下代码：
```bash
ssh root@你的主机ip或域名 -p 22
```
其中SSH是Secure Shell（安全外壳协议，简称SSH）的缩写，它采用加密的网络传输协议，可在不安全的网络中为网络服务提供安全的传输环境。任何网络服务都可以通过SSH实现安全传输，但SSH最常见的用途是远程登录系统。

`root@ip -p 22` 该字段表示“IP地址（ip）所指向服务器的22端口（-p 22）上所运行程序的用户（root）”，即唯一标识了自己云主机上CentOS系统运行的SSH服务与root用户。

由于SSH服务默认监听22端口，若没有手动修改过，`-p 22` 字段便可以省略。另外本地hosts文件中可以添加云服务器ip wordpressHost项目，下一次使用SSH登陆便可以直接采用如下语句：
```bash
ssh root@wordpressHost
```

另外对Linux主机来说，有许多免费强大的软件可以配合建立与服务器的连接，其中比较突出的有Xshell与Xftp，截止到2018.12该软件已更新到第六版。前者提供功能强大的命令行工具，后者提供文件传输的图形窗口。下载好软件之后，输入云主机ip、端口（ssh默认22，ftp默认20/21）、用户名（root）、密码等就可以与云服务器建立连接了。

### 2.2.3 常用命令

* 文件夹/文件建立/打开/编辑/删除

```bash
ls: 列出目录
cd：切换目录
pwd：显示目前的目录
mkdir：创建一个新的目录
rmdir：删除一个空的目录
cp: 复制文件或目录
rm: 移除文件或目录
mv: 移动文件与目录，或修改文件与目录的名称
```

可以使用 man [命令]来查看各个命令的使用文档，如 ：`man cp`

* 权限设置
```bash
useradd 选项 用户名
userdel 选项 用户名//常用的选项是 -r，它的作用是把用户的主目录一起删除
```
*注：Linux提供了集成的系统管理工具userconf，它可以用来对用户账号进行统一管理*

* 下载/安装/更新/卸载软件[Read More…]
	* linux yum 命令:yum（ Yellow dog Updater, Modified）是Shell前端软件包管理器。基於RPM包管理，能够从指定的服务器自动下载RPM包并且安装，可以自动处理依赖性关系，并且一次安装所有依赖的软体包，无须繁琐地一次次下载、安装。yum提供了查找、安装、删除某一个、一组甚至全部软件包的命令，而且命令简洁而又好记。指令格式为：
    ```bash
    yum [option] [command] [package [package]...]
    ```
    其中 options：可选，选项包括-h（帮助），-y（当安装过程提示选择全部为”yes”），-q（不显示安装的过程）等。command：要进行的操作。package：操作的对象。
    常用指令有：
    1. 列出所有可更新的软件清单命令：yum check-update
    2. 更新所有软件命令：yum update
    3. 仅安装指定的软件命令：yum install
    4. 仅更新指定的软件命令：yum update
    5. 列出所有可安裝的软件清单命令：yum list
    6. 删除软件包命令：yum remove
    7. 查找软件包 命令：yum search
    8. 清除缓存命令:
    ```bash
    yum clean packages: 清除缓存目录下的软件包
    yum clean headers: 清除缓存目录下的 headers
    yum clean oldheaders: 清除缓存目录下旧的 headers
    yum clean, yum clean all (= yum clean packages; yum clean oldheaders) :清除缓存目录下的软件包及旧的headers
    ```
    
示例：

```bash
[root@nginx ~]# yum install mysql
Setting up Install Process
Parsing package install arguments
Resolving Dependencies <==先检查软件的属性相依问题
--> Running transaction check...(省略)
```

更多参考：

[菜鸟教程](http://www.runoob.com)：全栈教程网站，适合有一定计算机理论基础的人阅读，排版简介、介绍简练，部分内容稍有老旧，可以作为手册型查阅网站使用。

[鸟哥的Linux私房菜](http://linux.vbird.org/)：对新手最友好的Linux学习网站之一。站主鸟哥（vbird）是台湾人，有根据网站内容整理出版的相关书籍，是新手程序员的不二选择。

# 3 配置服务端

## 3.1 服务器基本原理

### 3.1.1 什么是服务器

服务器是运行在网络主机（Network Host）上的计算机软件，可以管理资源并为用户提供服务。通常分为文件服务器（能使用户在其它计算机访问文件），数据库服务器和应用程序服务器。按照功能的不同，常见的服务器有（来自wiki）：

1. 文件服务器（File Server）或网络存储设备（Network Attached Storage）——如Novell的NetWare
2. 数据库服务器（Database Server）——如Oracle数据库服务器，MySQL，MariaDB，PostgreSQL，Microsoft SQL Server，MongoDB，Redis等
3. 邮件服务器（Mail Server）——Sendmail、Postfix、Qmail、Microsoft Exchange、Lotus Domino、dovecot等
4. 网页服务器（Web Server）——如Apache、lighttpd、nginx、微软的IIS等
5. FTP服务器（FTP Server）——Pureftpd、Proftpd、WU-ftpd、Serv-U、vs-ftpd等。
6. 域名服务器（DNS Server）——如Bind等
7. 应用程序服务器（Application Server/AP Server）——如Bea公司的WebLogic、JBoss、Sun的GlassFish
8. 代理服务器（Proxy Server）——如Squid cache
9. 计算机名称转换服务器——如微软的WINS服务器
10. 其他，如Minecraft游戏服务器等。

对网页服务器来说，每一个网页服务器程序监听80端口，从网络接受HTTP请求，然后提供HTTP回复给请求者。HTTP回复一般包含一个HTML文件（HyperText Markup Language，是一种标记语言，可以形成标准网页）的同时也可以包含纯文本文件、图像或其他类型的文件。一般来说这些文件都存储在网页服务器的本地文件系统里，当正确安装和设置好网页服务器软件后，服务器管理员需要指定一个本地路径名为网页文件的根目录。网页服务器便会在此目录中组织、查找、运行、编辑文件。

### 3.1.2 网络主机

网络主机是连接到一个计算机网络的一台计算机设备。网络主机可以向网络上的用户或其他节点提供资源、服务和应用。一台网络主机是已被分配一个网络层主机地址的网络节点。互联网主机会被分配一个或多个IP地址。该地址可以由管理员手动配置，也可以在启动时自动配置（通过DHCP等相关协议）。

RFC 871将一台主机定义为：“连接到一个通信网络的通用计算机系统，用于在参与的操作系统之间实现资源共享目的”。（RFC是Request For Comments的缩写，中文翻译是请求意见稿。它是由互联网工程任务组（IETF）发布的一系列备忘录。文件收集了有关互联网相关信息，以及UNIX和互联网社群的软件文件，以编号排定。可以认为RFC就是互联网标准制定的官方文件）

### 3.1.3 常见的网络服务器

现在市面上普遍的网页（HTTP）服务器有：

* Apache软件基金会的Apache HTTP服务器
    * Google基于Apache HTTP Server开发的的Google Web Server
* Windows系统下的Internet Information Server（IIS）
* Nginx公司的nginx
	* 淘宝从nginx改良的Tengine
* lighttpd公司的lighttpd
* Cherokee_(Web服务器)
* Microsoft的FrontPage

2018.12市场服务器占有率：（来自w3techs的数据）

Apache（44.6%）Nginx（40.7%）Microsoft-IIS（9.0%）LiteSpeed（3.7%）GoogleServer（0.9%）

这五种服务器在市场总占有率为98.9%，剩余服务器约为1.1%。其中Apache与IIS近几个月有小幅降低，Nginx有小幅增长。

## 3.2 安装Nginx

登陆Linux云主机后，输入以下指令：

`yum install nginx -y`

安装好后可以输入nginx -v命令来查询nginx版本。我安装的版本（2018.12）是：

<code>nginx version: nginx/1.12.2</code>

Nginx的配置文件为 default.conf，一般在/etc/nginx/conf.d/目录下，也可能直接在/etc/nginx/文件夹下。可以通过locate命令查看该文件位置：

```bash
[root@VM_0_11_centos ~]# locate nginx.conf
/etc/nginx/nginx.conf
/etc/nginx/nginx.conf.default
```

/etc/nginx/文件目录下一般还有/conf.d/和/default.d/文件夹，一般在conf.d文件夹中放置.conf 文件。.conf.default文是默认备份文件。可以通过nginx -t 命令查看nginx具体使用的配置文件：

```bash
[root@VM_0_11_centos ~]# nginx -t
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful
```

可见nginx实际调用的是/etc/nginx/nginx.conf 文件。我们可以通过Xftp软件（见上文中的Linux介绍部分）编辑该文件，也可以直接用vim命令来进行编辑。由于CentOS 6 不支持 IPv6，需要取消对 IPv6 地址的监听，否则 Nginx 不能成功启动。可以通过 cat /etc/redhat-release 指令查看本机Linux版本：

```bash
[root@VM_0_11_centos ~]# cat /etc/redhat-release
CentOS Linux release 7.5.1804 (Core)
```

我使用的是CentOS7.5。对CentOS6来说，若要去除对 IPv6 地址的监听（），可参考下面的代码来修改nginx.conf文件：

```
server {
listen 80 default_server;
# listen [::]:80 default_server;
server_name _;

#此处放置网页文件根目录
root /usr/share/nginx/html;

# Load configuration files for the default server block.
include /etc/nginx/default.d/*.conf;
location / {
}

error_page 404 /404.html;
location = /40x.html {
}
error_page 500 502 503 504 /50x.html;
location = /50x.html {
}
}
```

修改完成后，启动nginx：

`nginx`

若需要重启，则使用 `service nginx restart` 命令。此时，可以访问HTTP 服务（http://<云主机公网IP> ）来确认是否已经安装成功。

将 Nginx 设置为开机自动启动：
```bash
chkconfig nginx on
```

## 3.3 安装Apache

如果希望用Aphache代替Nginx做服务器也可以，其安装过程类似安装Nginx，简略如下：

```bash
#使用 yum 安装 Apache：
yum install httpd -y

#启动 Apache 服务：
service httpd start
```

## 3.4 安装IIS

IIS（Internet Information Services，互联网信息服务）是由微软公司提供的基于运行Microsoft Windows的互联网基本服务。最初是Windows NT版本的可选包，随后内置在Windows 2000、Windows XP Professional和Windows Server 2003一起发行，但在普遍使用的Windows XP Home版本上并没有IIS。

IIS包括FTP/FTPS、NNTP、和HTTPS／HTTP、SMTP等服务。 IIS可设置的内容包括：虚拟目录及访问权限、默认文件名称、以及是否允许浏览目录。

**Win2012下安装IIS：**

登陆云主机后，查看服务器管理器，点击添加角色和功能→安装类型：基于角色或基于功能的安装→服务器角色：web服务器（IIS）→角色服务：在默认基础上勾选CGI、ISAPI拓展、ISAPI筛选器。
配置安装完成后可以通过浏览器查看http://localhost/，以确定IIS是否配置正常。
使用IIS的云服务器一般都是Win2012系统，如果是使用win8及以上的更高系统，可以查看此处链接:win10下设置IIS

# 4 配置数据库

## 4.1 数据库基本原理

### 4.1.1 什么是数据库

数据库可视为电子化的文件柜——存储电子文件的地方，用户可以对文件中的数据运行新增、截取、更新、删除等操作。

数据库分为关系型数据库、非关系型数据库、键值数据库等，最常用的ySQL、MariaDB等为关系型数据库，其中的元素以“表（table）”的形式组织存储。表（关系Relation）是以列（属性Attribute）和行（值组Tuple）的形式组织起来的数据的集合。一个数据库包括一个或多个表（关系Relation）。

### 4.1.2 常见数据库产品

**关系型数据库：**
* MySQL
* MariaDB（MySQL的代替品）
* Microsoft Access
* Microsoft SQL Server
* Google Fusion Tables
* Oracle数据库

几乎所有的数据库管理系统都配备了一个开放式数据库连接（ODBC）驱动程序，令各个数据库之间得以互相集成。

*注意：Red Hat Enterprise Linux/CentOS 7.0 发行版已将默认的数据库从 MySQL 切换到 MariaDB*

**非关系型数据库（NoSQL）**

* BigTable（Google）
* Cassandra
* MongoDB
* CouchDB

**键值（key-value）数据库**

* Apache Cassandra（为Facebook所使用）：高度可扩展
* Dynamo
* LevelDB（Google）

### 4.1.3 数据库的基本操作

以root用户登录数据库：

```bash
mysql -u root -p
```

-u之后的root即用户名，回车后输入密码登陆。

以root权限登陆后运行以下命令来建立一个名字为wordpress的数据库：

```sql
create database wordpress;
```
然后建立一个用户名为wordpress、本地主机登陆、密码为Password的用户并将该数据库的读写权限授权给wordpress：

```sql
grant all privileges on wordpress.* to wordpress@'localhost' identified by "Password";
```

其语法格式为：

```sql
grant privilegesCode on dbName.tableName to username@host identified by "password";
```

privilegesCode表示授予的权限类型，常用的有以下几种类型：

* all privileges：所有权限。
* select：读取权限。
* delete：删除权限。
* update：更新权限。
* create：创建权限。
* drop：删除数据库、数据表权限。

dbName.tableName表示授予权限的具体库或表，常用的有以下几种选项：

* .：授予该数据库服务器所有数据库的权限。
* dbName.*：授予dbName数据库所有表的权限。
* dbName.dbTable：授予数据库dbName中dbTable表的权限。

username@host表示授予的用户以及允许该用户登录的IP地址。其中Host有以下几种类型：

* localhost：只允许该用户在本地登录，不能远程登录。
* %：允许在除本机之外的任何一台机器远程登录。
* 192.168.52.32：具体的IP表示只允许该用户从特定IP登录。

password指定该用户登录时的密码。

创建完成后运行`flush privileges;`命令进行刷新权限变更。

## 4.2 MySQL

### 4.2.1 Linux使用yum指令安装MySQL

```bash
#使用 yum 安装 MySQL：
yum install mysql-server -y

#安装完成后，启动 MySQL 服务：
service mysqld restart

#设置 MySQL 账户 root 密码：
/usr/bin/mysqladmin -u root password 'Password'
#此处示例的密码为Password。如果设置其它密码，请把密码记住。

#将 MySQL 设置为开机自动启动：
chkconfig mysqld on
```

### 4.2.2 Windows下安装MySQL

选择MySQL Installer，页面下方提供两种下载，一种是下载器，一种是完整文件。

下载较大的（MySQL8.0为313.8M）的完整文件，安装时Setup Types选择Server only-Type and Networking下的Config Type:Server Machine。安装好后进入mysql命令行，用“show database”命令查看是否配置好，“exit”命令退出。

## 4.3 MariaDB

由于CentOS从7.0发行版开始已将默认的数据库从 MySQL 切换到 MariaDB，所以我们可以用该数据库代替MySQL的server与client端。

类似安装MySQL数据库，MariaDB安装过程如下：

```bash
yum install MariaDB-server MariaDB-client -y

#启动MariaDB并设置开机自动启动：
systemctl start mariadb
systemctl enable mariadb

#下条指令查看mariadb的安装运行状态
systemctl status mariadb
```

## 4.4 phpMyAdmin

### 4.4.1 什么是phpMyAdmin

phpMyAdmin 是一个以PHP为基础，以Web-Base方式架构在网站主机上的MySQL的数据库管理工具，让管理者可用Web接口管理MySQL数据库。借由此Web接口可以成为一个简易方式输入繁杂SQL语法的较佳途径，尤其要处理大量数据的导入及导出更为方便。其中一个更大的优势在于由于phpMyAdmin跟其他PHP程序一样在网页服务器上运行，但是您可以在任何地方使用这些程序产生的HTML页面，也就是于远程管理MySQL数据库，方便的创建、修改、删除数据库及数据表。也可借由phpMyAdmin创建常用的php语法，方便编写网页时所需要的sql语法正确性。

### 4.4.2 Linux安装并配置phpMyAdmin

>注意：请在PHP配置好后再来配置phpMyAdmin

配置好MySQL、Nginx和PHP后，下载phpMyAdmin。

下载完成后将文件放置在/usr/share/php/文件夹下，并用unzip命令解压文件：

```bash
cd /usr/share/php
unzip phpMyAdmin-4.8.4-all-languages.zip
```

之后配置Ngnix服务器的conf文件。编辑/etc/nginx/conf.d/php.conf文件，将其编辑为：

```
server {
listen 8000;
#listen [::]:8000;
#server_name _;
root /usr/share/php/phpMyAdmin-4.8.4-all-languages;

# Load configuration files for the default server block.
#include /etc/nginx/default.d/*.conf;

location / {
index index.php index.html index.htm;
}

location ~ .php$ {
fastcgi_pass 127.0.0.1:9000;
#fastcgi_pass unix:/var/run/php-fpm.sock;
fastcgi_index index.php;
fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
include fastcgi_params;
}
}
```

再将/etc/nginx/nginx.conf文件中的server{}代码块修改为:

```
server {
listen 80 default_server;
#listen [::]:80 default_server;
server_name _;
root /usr/share/nginx/wordpress;

# Load configuration files for the default server block.
include /etc/nginx/default.d/*.conf;

location / {
index index.php index.html index.htm;
}


error_page 404 /404.html;
location = /40x.html {
}

error_page 500 502 503 504 /50x.html;
location = /50x.html {
}

location ~ .php$ {
fastcgi_pass 127.0.0.1:9000;
#fastcgi_pass unix:/var/run/php-fpm.sock;
fastcgi_index index.php;
fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
include fastcgi_params;
}
}
```

其中的root文件路径/usr/share/nginx/wordpress中即存放wordpress网页。

此时，访问 http://:8000/ 可浏览到phpMyAdmin 页面了。用户名和密码用于登陆MySQL。以后可以通过phpMyAdmin的导出与导入功能实现数据库备份与恢复。

# 5 配置PHP

## 5.1 PHP基本原理

### 5.1.1 PHP是怎么运行的

PHP从下到上是一个4层体系 ：

1. Zend引擎:Zend整体用纯c实现，是php的内核部分，它将php代码翻译（词法、语法解析等一系列编译过程）为可执行opcode的处理并实现相应的处理方法、实现了基本的数据结构（如hashtable、oo）、内存分配及管理、提供了相应的api方法供外部调用，是一切的核心，所有的外围功能均围绕zend实现。
2. Extensions
围绕着zend引擎，extensions通过组件式的方式提供各种基础服务，我们常见的各种内置函数（如array系列）、标准库等都是通过extension来实现，用户也可以根据需要实现自己的extension以达到功能扩展、性能优化等目的（如贴吧正在使用的php中间层、富文本解析就是extension的典型应用）。
3. Sapi:Sapi全称是Server Application Programming Interface，也就是服务端应用编程接口，sapi通过一系列钩子函数，使得php可以和外围交互数据，这是php非常优雅和成功的一个设计，通过sapi成功的将php本身和上层应用解耦隔离，php可以不再考虑如何针对不同应用进行兼容，而应用本身也可以针对自己的特点实现不同的处理方式。后面将在sapi章节中介绍
4. 上层应用:这就是我们平时编写的php程序，通过不同的sapi方式得到各种各样的应用模式，如通过webserver实现web应用、在命令行下以脚本方式运行等等。

### 5.1.2 PHP、php-fpm与Nginx

早期的webserver只处理html等静态文件，但是有时我们需要处理一个请求，按照合适的组织形式重新对网页进行布局。这是我们设计了CGI（Common Gateway Interface，通用网关接口）接口，它可以让一个客户端，从网页浏览器向执行在网络服务器上的程序请求数据。CGI的工作方式，从Web服务器的角度看，是在特定的url上定义了可以运行CGI程序。当收到一个匹配url的请求，相应的程序就会被调用，并将客户端发送的数据作为输入。程序的输出会由Web服务器收集，并加上合适的档头，再发送回客户端。

有了cgi协议，解决了php解释器与webserver通信的问题，webserver终于可以处理动态语言了。后面又出现了cgi的改良版本：fast-cgi，这也是现在最常用的处理动态网页的技术。

php-fpm即php-Fastcgi Process Manager。php-fpm是 FastCGI 的实现，并提供了进程管理的功能，包含 master 进程和 worker 进程两种进程。master 进程只有一个，负责监听端口，接收来自 Web Server 的请求，而 worker 进程则一般有多个(具体数量根据实际需要配置)，每个进程内部都嵌入了一个 PHP 解释器，是 PHP 代码真正执行的地方。

通过配置nginx的nginx.conf文件，我们可以将*.php文件交给php-fpm来处理，流程为：

www.cakipaul.com → Nginx服务器 → www.cakipaul.com/index.php → 因为是*.php文件，传送到php-fpm(:9000) → php-fpm 接收到请求，启用worker进程处理请求 → php-fpm 处理完请求，返回给Nginx服务器(:80) → nginx通过http将结果返回给服务器

## 5.2 安装PHP

### 5.2.1 Nginx服务器上安装PHP

```bash
#使用 yum 安装 PHP（php本身，fpm工具，与mysql工具）：
yum install php php-fpm php-mysql -y

#安装之后，启动 PHP-FPM 进程：
service php-fpm start

#启动之后，查看 PHP-FPM 进程监听哪个端口（默认为9000）
netstat -nlpt | grep php-fpm

#把 PHP-FPM 也设置成开机自动启动：
chkconfig php-fpm on
#注：CentOS 6 默认已经安装了 PHP-FPM 及 PHP-MYSQL，此命令执行的可能会提示已经安装。

```

### 5.2.2 配置 Nginx 并运行 PHP 程序

在 /etc/nginx/conf.d 目录中新建一个名为 php.conf 的文件，并配置 Nginx 端口 ，配置示例如下：

```
server {
listen 8000;
#pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
location ~ .php$ {
root /usr/share/php;
fastcgi_pass 127.0.0.1:9000;
fastcgi_index index.php;
fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
include fastcgi_params;
}
}
```

修改配置完成后，重启 nginx 服务:

```bash
service nginx restart
```

在/usr/share/php 目录下新建一个 info.php 文件来检查 php 是否安装成功了，文件内容参考如下：

```php
<?php
        phpinfo();
    ?>
```

此时，访问 http://:8000/info.php 可浏览到刚刚创建的 info.php 页面了。

### 4.2.3 Apache服务器上安装并配置PHP

安装 PHP 和 PHP-MYSQL 支持工具：

使用 yum 安装 PHP：

>注：CentOS 6 默认已经安装了 php-mysql，下面命令执行的可能会提示已经安装。

```bash
yum install php php-mysql -y
```
可以在 /var/www/html 目录下创建一个info.php文件来检查php是否安装成功，示例代码参考如下：

```php
<?php
        phpinfo();
    ?>
```

重启 Apache 服务：

```bash
service httpd restart
```

此时，访问 http:///info.php 就可浏览到刚刚创建的 info.php 页面了。

### 5.2.4 PHP5.4升级5.6

参考：[如何在CentOS上升级php5.4至5.6？](https://towait.com/blog/how-to-upgrade-php-version-54-to-56-on-cento7/)

>注意：不要忘了安装php-fpm

```bash
yum install php56w-fpm6 
```

# 6 安装WordPress

## 6.1 快速配置

在数据库中建立wordpress数据库（数据库名称可以自主拟定）并新建相关用户、给予权限（除非你打算让wordpress直接使用root权限），这部分内容参考上文中的*数据库的基本操作*。

建立好数据库与访问该数据库的账户之后，使用Xftp软件将wordpress安装包放置在`/usr/share/nginx/`文件夹下并使用unzip命令解压,将解压后、包含wordpress完整文件的文件夹重命名，即使网站根目录为`/usr/share/nginx/wordpress`。

配置nginx.conf文件中的root项：

```
root /usr/share/nginx/wordpress;
```

下一步便是配置wp-config.php文件了。

## 6.2 配置wp-config.php文件

打开路径/usr/share/nginx/wordpress，可以看到wp-config-sample.php文件。将其重命名为wp-config.php，打开并编辑：

```
// ** MySQL 设置 - 具体信息来自您正在使用的主机 ** //
/** WordPress数据库的名称 */
define('DB_NAME', '数据库名');

/** MySQL数据库用户名 */
define('DB_USER', '用户名');

/** MySQL数据库密码 */
define('DB_PASSWORD', '密码');

/** MySQL主机 */
define('DB_HOST', 'localhost');

/** 创建数据表时默认的文字编码 */
define('DB_CHARSET', 'utf8');

/** 数据库整理类型。如不确定请勿更改 */
define('DB_COLLATE', '');
```

另外可以编辑下方的八个define中的身份认证密钥与盐，将其中define('', '');第二个单引号之间的内容填上随机的字符串并保存即可。

配置完成后还需要将网站页面初始化。访问页面：http://<云主机 IP>/wp-admin/install.php，即可运行初始化。初始化完成后直接登陆http://<云主机 IP>就可以进入wordpress的主页了。

## 6.3 拓展html/css推荐

在自行探索WordPress的主页布局后，如果我们希望进行个性化设计，WordPress也提供有相应的拓展模块。在自定义-小工具-侧边栏中，我们可以添加自定义的html代码。下面以FlagCounter（本站使用了该服务）为例：

访问(Flag Counter)[https://flagcounter.com/],设置好配色与布局显示格式后，点击“GET YOUR FLAG COUNTER”就可以获取属于自己的站点统计html代码。将显示的两个代码中的“Code for websites (HTML)”中的代码复制，粘贴到WordPress的自定义HTML中，即可根据访客IP显示出各国人数统计。

# 7 安装插件

## 7.1 路径权限设置

如果安装、更新插件或主题时提示ftp服务器出现问题，一般是因为操作权限问题。下面给出解决方案：

首先进入 网站的根目录下：（如usr/share/nginx/wordpress）

```bash
cd /wordpress所在目录
```

接着，给wordpress整个文件夹进行赋值权限，如果不赋予权限 更新的时候会报权限不足的错误(root 权限下进行)

```bash
chmod -R 777 /wordpress所在目录
```

然后： 进入wordpress 文件夹里面。修改wp-config.php 文件

```bash
vim   wp-config.php

```
添加代码（添加到任意位置，建议添加到页尾）:

```php
define("FS_METHOD", "direct");  
define("FS_CHMOD_DIR", 0777);  
define("FS_CHMOD_FILE", 0777);
```

`:wq` 保存文件，重启apache或nginx服务：

```bash
service httpd restart
#or
service nginx restart
```

刷新界面进行更新

## 7.2 插件推荐

### 7.2.1 安全插件

* Akismet Anti-Spam：反爬虫软件，需要绑定Akismet账户（可免费注册），具体使用方法参照安装后的设置界面；
* Limit Login Attempts：限制登陆尝试次数插件；
* WP Mail SMTP：优秀的SMTP邮件管理插件。

### 7.2.2 功能与美化插件

* Timeline Express：提供时间轴小工具；
* Quotes Collection：实用的名人名言随机展示工具（需要自己添加条目）；
* Easy Table of Contents：TOC插件，按照标题级别自动显示文章目录导航；
* WP Super Cache：轻量级快速缓存插件
* Enlighter – Customizable Syntax Highlighter：代码块高亮插件，支持多种编程、脚本、标记语言；
* JP Markdown：经典的MarkDown编辑器，可配合Enlighter插件使用；
* WP Editor.md：国人制作的最好的支持MarkDown语法、emoj、LaTex等的编辑器，支持实时预览。拓展阅读：[什么是MarkDown?]
