---
title: 防范云主机ssh暴力攻击
date: 2019-01-10 13:00:00
tags:
- web
- linux
- ssh
categories:
- 操作系统
---
# 1 缘起
最近ssh登录的时候，主机提示距离上次登录之间遇到过n次失败登陆，于是运行指令查看log文件：
```bash
cat /var/log/secure|grep "Failed"
```
结果刷刷刷下来几百上千条记录，真的是字面意义的暴力破解，什么用户名和端口都给试了一遍，下面是随机截取的五次失败记录：

```bash
Jan  1 11:11:03 VM_0_2_centos sshd[16734]: Failed password for invalid user admin from 159.89.13.0 port 41382 ssh2
Jan  1 11:11:42 VM_0_2_centos sshd[16796]: Failed password for invalid user cloud-user from 119.29.39.236 port 51182 ssh2
Jan  1 11:12:02 VM_0_2_centos sshd[16815]: Failed password for invalid user bkp from 106.75.209.173 port 35982 ssh2
Jan  1 11:14:39 VM_0_2_centos sshd[17037]: Failed password for invalid user aaUser from 84.118.75.49 port 48034 ssh2
Jan  1 11:14:50 VM_0_2_centos sshd[17049]: Failed password for invalid user derek from 106.75.209.173 port 47404 ssh2
```

获取其中的ip地址和数量:
```bash
grep -o '[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}' /var/log/secure | sort | uniq -c
```
结果列表之长惊了我一身冷汗，几个页面都没显示完全。IP开头从23到223应有尽有，每个IP都有几十次尝试，不知道是哪位黑客手头这么多肉鸡，下面是冰山一角：
```bash
     40 54.36.151.64
     40 54.36.189.143
     22 54.36.189.240
     21 54.36.95.101
     15 54.37.149.175
     35 54.37.154.254
```
# 2 CentOS 7修改SSH端口并配置iptables防火墙

## 2.1 修改ssh端口
```bash
#1.修改 sshd_config 端口
vi /etc/ssh/sshd_config


#2.取消 #Port 22 的注释，在下一行添加你需要修改的新端口 Port 2048。（这里不删除 22 端口是为了防止修改后新端口无法访问，造成无法用 ssh 连接服务器。）
Port 22  
Port 2048  

#3.修改保存 sshd_config 文件后重启 sshd 服务：
systemctl restart sshd

#4.退出 ssh 会话后，再用新的端口连接：
ssh -p 2048 root@example.com
```
## 2.2 安装semanage工具
SELinux 全称 Security Enhanced Linux (安全强化 Linux)，是 MAC (Mandatory Access Control，强制访问控制系统)的一个实现，目的在于明确的指明某个进程可以访问哪些资源(文件、网络端口等)。对于 ssh，SELinux 默认只允许 22 端口，我们可以用 SELinux 管理配置工具 semanage，来修改 ssh 可访问的端口。

```bash
#1. 安装 semanage 工具
yum provides semanage
yum -y install policycoreutils-python

#2. 为 ssh 打开 2048 端口
semanage port -a -t ssh_port_t -p tcp 2048

#3. 查看当前 SELinux 允许的端口
$ semanage port -l | grep ssh
ssh_port_t                     tcp      2048, 22  

#4. 修改 /etc/selinux/config 配置，启用 SELinux：
vi /etc/selinux/config

SELINUX=permissive  

#5. 重启服务器
init 6

#6. 重启后查看 SELinux 状态
sestatus
# if it shows disable, you can run
load_policy -qi

#7. 检查配置
semanage port -a -t ssh_port_t -p tcp 2048
semanage port -l | grep ssh
ssh_port_t                     tcp      2048, 22  

#8. 重启 ssh 服务
systemctl restart sshd
```
*注：semange 不能禁用 ssh 的 22 端口：*
```bash
semanage port -d -t ssh_port_t -p tcp 22
ValueError: 在策略中定义了端口 tcp/22，无法删除。  
```

## 2.3 使用firewall-cmd禁用22端口连接ssh

```bash
#1. 配置防火墙 firewalld启用防火墙 && 查看防火墙状态
systemctl enable firewalld
systemctl start firewalld
systemctl status firewalld
firewall-cmd --state

#2. 查看防火墙当前「默认」和「激活」zone（区域）
firewall-cmd --get-default-zone
public  

firewall-cmd --get-active-zones
public  
  interfaces: eth0 eth1

#3.若没有激活区域的话，要执行下面的命令。激活 public 区域，增加网卡接口
firewall-cmd --set-default-zone=public
firewall-cmd --zone=public --add-interface=eth0
success  

firewall-cmd --zone=public --add-interface=eth1
success  

#4. 为 public zone 永久开放 2048/TCP 端口：
#注意：以防新端口不生效，先把 22 端口暴露
firewall-cmd --permanent --zone=public --add-port=22/tcp
firewall-cmd --permanent --zone=public --add-port=2048/tcp
#注意作为服务器还要添加80、ssl443、ftp的21/22等端口

#5. 重载防火墙
firewall-cmd --reload

#6. 查看暴露端口规则
firewall-cmd --permanent --list-port
443/tcp 80/tcp 22/tcp 2048/tcp  

firewall-cmd --zone=public --list-all
public (default, active)  
  interfaces: eth0 eth1
  sources:
  services: dhcpv6-client ssh
  ports: 443/tcp 80/tcp 22/tcp 2048/tcp
  masquerade: no
  forward-ports:
  icmp-blocks:
  rich rules:
```
退出 ssh 后，尝试连接新端口：
`ssh -p 2048 root@example.com`
成功登录的话，就可以做收尾工作了。

# 3 参考
[CentOS 7修改SSH端口并配置iptables防火墙](https://www.centos.bz/2017/08/centos-7-change-ssh-port/)
[修改ssh端口及iptables设置](http://coolnull.com/282.html) *（注：这篇参考文章内容较旧，仅作参考）*